# Backoffice 
The goal is to create an application to help myself with my company requirements.

This is a mono repository and contains infra + backend + frontend
```
QUARKUS_DATASOURCE_USERNMAE=XXX
QUARKUS_DATASOURCE_PASSWORD=XXX
QUARKUS_DATASOURCE_JDBC_URL=XXX
QUARKUS_LAMBDA_HANDLER=XXX
JAVA_TOOL_OPTIONS=-XX:+TieredCompilation -XX:TieredStopAtLevel=1.
```


## Backend
The backend is build with Quarkus.
For local development there is a application-http module.
The production environment contains of 2 aws lambda functions.
```
COGNITO_CLIENT_ID=XXX
COGNITO_CLIENT_SECRET=XXX
COGNITO_ISSUER=XXX

NEXTAUTH_URL=XXX
NEXTAUTH_SECRET=XXX

NEXT_PUBLIC_BACKOFFICE_API_URL=XXX
```

## Frontend
The frontend is build with next.js and tailwind css.
It is deployed on Vercel.
Authentication is done via next auth. and requires some environment variables:

```
COGNITO_CLIENT_ID=XXX
COGNITO_CLIENT_SECRET=XXX
COGNITO_ISSUER=XXX

NEXTAUTH_URL=XXX
NEXTAUTH_SECRET=XXX

NEXT_PUBLIC_BACKOFFICE_API_URL=XXX
```

Local development => `npm run dev`

## Infra
The infrastructure can be applied to ans aws cloud environment => `terrafrom apply`