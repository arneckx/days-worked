# backoffice

This project uses Quarkus, the Supersonic Subatomic Java Framework.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

## Local development

Run a postgresql container:
`docker run --name postgres -p 5432:5432 -e POSTGRES_PASSWORD=password -d postgres:15.4-alpine`


## Deployment

Do not change the handler switch. This must be hardcoded to `io.quarkus.amazon.lambda.runtime.QuarkusStreamHandler::handleRequest`.  
This handler bootstraps Quarkus and wraps your actual handler so that injection can be performed.