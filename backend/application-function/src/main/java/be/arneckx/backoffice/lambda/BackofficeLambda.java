package be.arneckx.backoffice.lambda;

import be.arneckx.backoffice.command.HttpCommandAdapter;
import be.arneckx.backoffice.command.Response;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Optional;

@ApplicationScoped
@Named("backoffice")
public class BackofficeLambda implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

    private static final Logger LOGGER = LoggerFactory.getLogger(BackofficeLambda.class);

    private final HttpCommandAdapter httpCommandAdapter;
    private final ObjectMapper mapper;

    public BackofficeLambda(HttpCommandAdapter httpCommandAdapter, ObjectMapper mapper) {
        this.httpCommandAdapter = httpCommandAdapter;
        this.mapper = mapper;
    }

    @Override
    public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent event, Context context) {
        try {
            String commandHeader =  Optional.ofNullable(event.getHeaders().get(HttpCommandAdapter.HTTP_CUSTOM_COMMAND_HEADER))
                    .or(() -> Optional.ofNullable(event.getHeaders().get(HttpCommandAdapter.HTTP_CUSTOM_COMMAND_HEADER.toLowerCase())))
                    .orElseThrow(() -> new RuntimeException(String.format("%s is required as header", HttpCommandAdapter.HTTP_CUSTOM_COMMAND_HEADER)));
            long start = System.currentTimeMillis();
            LOGGER.info("Receiving request with header {}", commandHeader);
            Response response = httpCommandAdapter.handle(commandHeader, event.getBody());
            LOGGER.info("Executed command {} in {}ms", commandHeader, System.currentTimeMillis() - start);
            return new APIGatewayProxyResponseEvent()
                    .withStatusCode(201)
                    .withBody(mapper.writeValueAsString(response))
                    .withHeaders(Map.of(
                            "Access-Control-Allow-Origin", "*",
                            "Content-Type", "application/json"
                    ))
                    .withIsBase64Encoded(false);
        } catch (JsonProcessingException jsonException) {
            return new APIGatewayProxyResponseEvent()
                    .withStatusCode(500);
        }
    }

}
