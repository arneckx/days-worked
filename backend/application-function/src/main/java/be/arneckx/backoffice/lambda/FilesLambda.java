package be.arneckx.backoffice.lambda;

import be.arneckx.backoffice.command.HttpCommandAdapter;
import be.arneckx.backoffice.command.Response;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Named;
import org.apache.commons.fileupload.MultipartStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

@ApplicationScoped
@Named("files")
public class FilesLambda implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

    private static final Logger LOGGER = LoggerFactory.getLogger(FilesLambda.class);

    private final HttpCommandAdapter httpCommandAdapter;
    private final ObjectMapper mapper;

    public FilesLambda(HttpCommandAdapter httpCommandAdapter, ObjectMapper mapper) {
        this.httpCommandAdapter = httpCommandAdapter;
        this.mapper = mapper;
    }

    @Override
    public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent event, Context context) {
        try {
            String commandHeader = Optional.ofNullable(event.getHeaders().get(HttpCommandAdapter.HTTP_CUSTOM_COMMAND_HEADER))
                    .or(() -> Optional.ofNullable(event.getHeaders().get(HttpCommandAdapter.HTTP_CUSTOM_COMMAND_HEADER.toLowerCase())))
                    .orElseThrow(() -> new RuntimeException(String.format("%s is required as header", HttpCommandAdapter.HTTP_CUSTOM_COMMAND_HEADER)));
            long start = System.currentTimeMillis();
            LOGGER.info("Receiving request with header {}", commandHeader);
            Response response = withMultiPartFile(event, inputStream -> httpCommandAdapter.handle(commandHeader, inputStream));
            LOGGER.info("Executed command {} in {}ms", commandHeader, System.currentTimeMillis() - start);
            return new APIGatewayProxyResponseEvent()
                    .withStatusCode(201)
                    .withBody(mapper.writeValueAsString(response))
                    .withHeaders(Map.of(
                            "Access-Control-Allow-Origin", "*",
                            "Content-Type", "application/json"
                    ))
                    .withIsBase64Encoded(false);
        } catch (JsonProcessingException jsonException) {
            return new APIGatewayProxyResponseEvent()
                    .withStatusCode(500);
        }
    }

    public Response withMultiPartFile(APIGatewayProxyRequestEvent event, Function<InputStream, Response> function) {
        try {
            Map<String, String> headers = event.getHeaders();
            String contentType = headers.get("content-type");
            String[] boundaryArray = contentType.split("=");
            byte[] boundary = boundaryArray[1].getBytes();
            ByteArrayInputStream content = new ByteArrayInputStream(event.getBody().getBytes());
            MultipartStream multipartStream = new MultipartStream(content, boundary, event.getBody().getBytes().length, null);

            //Create a ByteArrayOutputStream
            ByteArrayOutputStream out = new ByteArrayOutputStream();

            //Find first boundary in the MultipartStream
            boolean nextPart = multipartStream.skipPreamble();

            //Loop through each segment
            LOGGER.info("Starting looping over multipart stream");
            while (nextPart) {
                String header = multipartStream.readHeaders();

                LOGGER.info("Headers:");
                LOGGER.info(header);

                multipartStream.readBodyData(out);
                nextPart = multipartStream.readBoundary();
            }

            //Log completion of MultipartStream processing
            LOGGER.info("Data written to ByteStream");

            //Prepare an InputStream from the ByteArrayOutputStream
            InputStream fis = new ByteArrayInputStream(out.toByteArray());

            return function.apply(fis);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
