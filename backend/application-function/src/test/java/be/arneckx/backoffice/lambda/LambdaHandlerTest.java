package be.arneckx.backoffice.lambda;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.response.ResponseBodyExtractionOptions;
import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static io.restassured.RestAssured.given;

@QuarkusTest
class LambdaHandlerTest {

    @Test
     void testLambdaSuccess() throws IOException, JSONException {
        String request = contentOf("request.json");
        String expectedResponse = contentOf("expectedResponse.json");

        ResponseBodyExtractionOptions responseBody = given()
                .contentType("application/json")
                .accept("application/json")
                .body(request)
                .when()
                .post()
                .then()
                .statusCode(200)
                .extract()
                .body();

        JSONAssert.assertEquals(expectedResponse, responseBody.jsonPath().prettyPrint(), JSONCompareMode.NON_EXTENSIBLE);
    }

    private String contentOf(String fileName) throws IOException {
        File file = new File(getClass().getClassLoader().getResource(fileName).getFile());
        return Files.readString(Path.of(file.getAbsolutePath()));
    }

}
