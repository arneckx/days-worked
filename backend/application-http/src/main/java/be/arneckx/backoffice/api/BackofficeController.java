package be.arneckx.backoffice.api;

import be.arneckx.backoffice.command.HttpCommandAdapter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import org.jboss.resteasy.reactive.ResponseStatus;
import org.jboss.resteasy.reactive.RestForm;
import org.jboss.resteasy.reactive.RestHeader;
import org.jboss.resteasy.reactive.multipart.FileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import static jakarta.ws.rs.core.MediaType.MULTIPART_FORM_DATA;

@Path("/api/backoffice")
@ApplicationScoped
public class BackofficeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(BackofficeController.class);

    private final HttpCommandAdapter httpCommandAdapter;
    private final ObjectMapper mapper;

    @Inject
    BackofficeController(
            HttpCommandAdapter httpCommandAdapter,
            ObjectMapper mapper
    ) {
        this.httpCommandAdapter = httpCommandAdapter;
        this.mapper = mapper;
    }

    @POST
    @ResponseStatus(201)
    @Produces({MediaType.APPLICATION_JSON})
    public String accept(@RestHeader(HttpCommandAdapter.HTTP_CUSTOM_COMMAND_HEADER) String commandHeader, String body) throws JsonProcessingException {
        LOGGER.info("Receiving request with header {}", commandHeader);
        String result = mapper.writeValueAsString(httpCommandAdapter.handle(commandHeader, body));
        LOGGER.info("Executed command {}", commandHeader);
        return result;
    }

    @Path("/files")
    @POST
    @Consumes(MULTIPART_FORM_DATA)
    @ResponseStatus(201)
    @Produces({MediaType.APPLICATION_JSON})
    public String acceptFile(@RestHeader(HttpCommandAdapter.HTTP_CUSTOM_COMMAND_HEADER) String commandHeader, @RestForm("file") FileUpload file) {
        LOGGER.info("Receiving file upload request with header {}", commandHeader);
        try (InputStream is = new FileInputStream(file.uploadedFile().toFile())) {
            String result = mapper.writeValueAsString(httpCommandAdapter.handle(commandHeader, is));
            LOGGER.info("Executed file upload command {}", commandHeader);
            return result;
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }
}
