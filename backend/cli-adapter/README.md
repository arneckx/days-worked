# Worked days in specified month

Utility to calculate worked days in month. Weekends are not taken into account at this moment.

Arguments:

- arg 1: month (e.g. AUGUST)
- arg 2: days not worked (e.g. 8,9,10,11,12,15)

Fish function
```
function days-worked --description 'Show the worked days in a month'

    sdk use java 18.0.2-amzn && java -Dfile.encoding=UTF-8 -classpath {{projectDirectory}}/backoffice/backend/cli-adapter/target/classes:{{projectDirectory}}/backoffice/backend/core/target/classes be.arneckx.backoffice.cli.calendar.Main $argv[1] $argv[2]

end
```

Output:

```
2022-08-01 Mon
2022-08-02 Tue
2022-08-03 Wed
2022-08-04 Thu
2022-08-05 Fri


xxxxxxxxxxxxxx
xxxxxxxxxxxxxx
xxxxxxxxxxxxxx
xxxxxxxxxxxxxx
xxxxxxxxxxxxxx


xxxxxxxxxxxxxx
2022-08-16 Tue
2022-08-17 Wed
2022-08-18 Thu
2022-08-19 Fri


2022-08-22 Mon
2022-08-23 Tue
2022-08-24 Wed
2022-08-25 Thu
2022-08-26 Fri


2022-08-29 Mon
2022-08-30 Tue
2022-08-31 Wed

Total of worked days in August is 17
```