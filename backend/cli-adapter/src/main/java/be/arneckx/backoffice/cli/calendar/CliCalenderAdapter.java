package be.arneckx.backoffice.cli.calendar;

import be.arneckx.backoffice.core.calendar.AmountOfDay;
import be.arneckx.backoffice.core.calendar.CalendarInfrastructureGateway;
import be.arneckx.backoffice.core.calendar.Offday;
import be.arneckx.backoffice.core.calendar.OffdayType;

import java.time.Month;
import java.time.Year;
import java.time.YearMonth;
import java.util.List;
import java.util.Set;

public class CliCalenderAdapter implements CalendarInfrastructureGateway {

    List<Offday> offdays;

    public CliCalenderAdapter(Year year, Month month, List<Integer> offDays) {
        this.offdays = offDays.stream()
                .map(day -> new Offday(
                        day,
                        YearMonth.of(year.getValue(), month),
                        "",
                        OffdayType.VACATION,
                        AmountOfDay.FULL
                ))
                .toList();
    }

    @Override
    public void add(Offday offday) {
        offdays.add(offday);
    }

    @Override
    public List<Offday> findDaysOffFor(Set<YearMonth> months) {
        return offdays;
    }

    @Override
    public List<Offday> findDaysOffFor(int year) {
        return offdays;
    }

    @Override
    public void delete(YearMonth yearMonth, int day) {
        offdays.stream()
                .filter(offday -> offday.yearMonth().equals(yearMonth) && offday.day() == day)
                .findAny()
                .ifPresent(presentOffday -> offdays.remove(presentOffday));
    }
}
