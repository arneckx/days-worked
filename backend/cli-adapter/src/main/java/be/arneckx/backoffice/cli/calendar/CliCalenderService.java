package be.arneckx.backoffice.cli.calendar;

import be.arneckx.backoffice.core.calendar.CalendarService;
import be.arneckx.backoffice.core.calendar.MonthlyOverview;

import java.time.Month;
import java.time.Year;
import java.time.YearMonth;
import java.util.List;
import java.util.Set;

public class CliCalenderService {

    private final CalendarService offdayService;
    private final Year year;
    private final Month month;

    public CliCalenderService(Year year, Month month, List<Integer> offdays) {
        this.year = year;
        this.month = month;
        offdayService = new CalendarService(new CliCalenderAdapter(year, month, offdays));
    }

    public String  overview() {
        MonthlyOverview monthlyOverview = offdayService.findDaysOffFor(Set.of(YearMonth.of(year.getValue(), month))).stream().findFirst().orElseThrow();
        return monthlyOverview.asString();
    }
}
