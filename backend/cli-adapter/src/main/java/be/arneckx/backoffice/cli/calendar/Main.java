package be.arneckx.backoffice.cli.calendar;

import java.time.Month;
import java.time.Year;
import java.util.Arrays;

class Main {

    public static void main(String[] args) {
        Year year = Year.now();
        Month month = Month.valueOf(args[0]);

        CliCalenderService calender = new CliCalenderService(year, month, Arrays.stream(args[1].split(",")).map(Integer::parseInt).toList());

        System.out.println(calender.overview());
    }
}