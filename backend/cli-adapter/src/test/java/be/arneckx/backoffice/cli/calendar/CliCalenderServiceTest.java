package be.arneckx.backoffice.cli.calendar;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Test;

import java.time.Month;
import java.time.Year;
import java.util.List;

class CliCalenderServiceTest {

    @Test
    void workedDaysAugust2023() {
        CliCalenderService workedDays = new CliCalenderService(Year.of(2023), Month.AUGUST, List.of(8, 9, 10, 11, 12, 15));

        String actualOutput = workedDays.overview();

        assertThat(actualOutput).isEqualTo("""
                2023-08-01 Tue
                2023-08-02 Wed
                2023-08-03 Thu
                2023-08-04 Fri
                                
                                
                2023-08-07 Mon
                xxxxxxxxxxxxxx
                xxxxxxxxxxxxxx
                xxxxxxxxxxxxxx
                xxxxxxxxxxxxxx
                                
                                
                2023-08-14 Mon
                xxxxxxxxxxxxxx
                2023-08-16 Wed
                2023-08-17 Thu
                2023-08-18 Fri
                                
                                
                2023-08-21 Mon
                2023-08-22 Tue
                2023-08-23 Wed
                2023-08-24 Thu
                2023-08-25 Fri
                                
                                
                2023-08-28 Mon
                2023-08-29 Tue
                2023-08-30 Wed
                2023-08-31 Thu
                                
                Total of worked days in August is 18""");
    }

}