package be.arneckx.backoffice.core;

import java.time.Clock;
import java.time.ZoneId;

public final class BackofficeClock {

    private BackofficeClock() {}

    public static final String EUROPE_BRUSSELS = "Europe/Brussels";
    public static final Clock CLOCK = Clock.system(ZoneId.of(EUROPE_BRUSSELS));
}