package be.arneckx.backoffice.core;

public interface ThrowingFunction<T, R> {
    R apply(T t) throws Exception;
}
