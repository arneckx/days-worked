package be.arneckx.backoffice.core.calendar;

import java.util.Arrays;

public enum AmountOfDay {
    FULL(1f),
    HALF(0.5f),
    QUARTER(0.25f),
    ;

    private final float decimal;

    AmountOfDay(float decimal) {
        this.decimal = decimal;
    }

    public static AmountOfDay of(float amount) {
        return Arrays.stream(values())
                .filter(amountOfDay -> Float.compare(amountOfDay.decimal, amount) == 0)
                .findFirst()
                .orElseThrow();
    }

    public float asDecimal() {
        return decimal;
    }
}
