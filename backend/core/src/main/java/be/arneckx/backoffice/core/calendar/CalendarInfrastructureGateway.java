package be.arneckx.backoffice.core.calendar;

import java.time.YearMonth;
import java.util.List;
import java.util.Set;

public interface CalendarInfrastructureGateway {

    void add(Offday offday);

    List<Offday> findDaysOffFor(Set<YearMonth> months);

    List<Offday> findDaysOffFor(int year);

    void delete(YearMonth yearMonth, int day);
}
