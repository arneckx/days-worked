package be.arneckx.backoffice.core.calendar;

import be.arneckx.backoffice.core.BackofficeClock;
import jakarta.enterprise.context.ApplicationScoped;

import java.time.YearMonth;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

@ApplicationScoped
public class CalendarService {

    private final CalendarInfrastructureGateway calendarGateway;

    public CalendarService(CalendarInfrastructureGateway calendarGateway) {
        this.calendarGateway = calendarGateway;
    }

    public void add(Offday offday) {
        calendarGateway.add(offday);
    }

    public List<MonthlyOverview> findDaysOffFor(Set<YearMonth> months) {
        List<Offday> daysOff = calendarGateway.findDaysOffFor(months);
        Map<YearMonth, List<Offday>> daysOffByYearMonth = daysOff.stream().collect(Collectors.groupingBy(Offday::yearMonth));
        return months.stream()
                .map(yearMonth -> new MonthlyOverview(yearMonth, daysOffByYearMonth.getOrDefault(yearMonth, List.of())))
                .collect(toList());
    }

    public void delete(YearMonth yearMonth, int day) {
        calendarGateway.delete(yearMonth, day);
    }

    public CalendarYearOverview overviewFor(int year) {
        List<Offday> daysOff = calendarGateway.findDaysOffFor(year);
        return new CalendarYearOverview(year, daysOff, BackofficeClock.CLOCK);
    }
}
