package be.arneckx.backoffice.core.calendar;

import java.time.*;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CalendarYearOverview {

    private final float totalWorkedDays;
    private final float expectedWorkedDays;
    private final float totalOffdays;

    public CalendarYearOverview(int year, List<Offday> offdays, Clock clock) {
        LocalDate today = LocalDate.now(clock);
        float totalWorkedDays = 0f;
        float expectedWorkedDays = 0f;

        Map<DayMonth, Offday> offdaysByDay = offdays.stream().collect(Collectors.toMap(offday -> new DayMonth(offday.day(), offday.yearMonth().getMonthValue()), Function.identity()));
        List<LocalDate> allDaysOfTheYear = YearMonth.of(year, Month.JANUARY).atDay(1)
                .datesUntil(YearMonth.of(year, Month.DECEMBER).atEndOfMonth().plusDays(1))
                .toList();

        for (LocalDate dayOfMonth : allDaysOfTheYear) {
            if (isWeekend(dayOfMonth)) {
                continue;
            }

            float amountOfDayWorked = amountOfDayWorked(dayOfMonth, offdaysByDay);
            expectedWorkedDays+=amountOfDayWorked;
            if (dayOfMonth.isBefore(today)) {
                totalWorkedDays+=amountOfDayWorked;
            }
        }

        this.totalWorkedDays = totalWorkedDays;
        this.expectedWorkedDays = expectedWorkedDays;
        totalOffdays = (float) offdays.stream().mapToDouble(offday -> offday.amount().asDecimal()).sum();
    }

    private float amountOfDayWorked(LocalDate dayOfMonth, Map<DayMonth, Offday> offdaysByDay) {
        DayMonth dayMonth = new DayMonth(dayOfMonth.getDayOfMonth(), dayOfMonth.getMonthValue());
        if (offdaysByDay.containsKey(dayMonth)) {
            Offday offday = offdaysByDay.get(dayMonth);
            return  1.0f - offday.amount().asDecimal();
        } else {
            return 1f;
        }
    }

    private boolean isWeekend(LocalDate dayOfMonth) {
        return dayOfMonth.getDayOfWeek() == DayOfWeek.SATURDAY || dayOfMonth.getDayOfWeek() == DayOfWeek.SUNDAY;
    }

    public float totalWorkedDays() {
        return totalWorkedDays;
    }

    public float expectedWorkedDays() {
        return expectedWorkedDays;
    }

    public float totalOffdays() {
        return totalOffdays;
    }

    private record DayMonth(int day, int month) {}
}
