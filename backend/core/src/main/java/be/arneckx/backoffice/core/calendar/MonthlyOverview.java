package be.arneckx.backoffice.core.calendar;

import java.text.DecimalFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class MonthlyOverview {

    private final YearMonth monthWithYear;
    private final List<Offday> offdays;
    private final float numberOfWorkedDays;
    private final String stringOutput;

    public MonthlyOverview(YearMonth monthWithYear, List<Offday> offdays) {
        String NEW_LINE = "\n";
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd E");

        float numberOfWorkedDays = 0f;
        StringBuilder outputBuilder = new StringBuilder();

        Map<Integer, Offday> offdaysByDay = offdays.stream().collect(Collectors.toMap(Offday::day, Function.identity()));

        for (LocalDate dayOfMonth : monthWithYear.atDay(1).datesUntil(monthWithYear.atEndOfMonth().plusDays(1)).toList()) {
            if (isWeekend(dayOfMonth)) {
                outputBuilder.append(NEW_LINE);
            } else {
                if (offdaysByDay.containsKey(dayOfMonth.getDayOfMonth())) {
                    Offday offday = offdaysByDay.get(dayOfMonth.getDayOfMonth());

                    switch (offday.amount()) {
                        case FULL -> outputBuilder.append("xxxxxxxxxxxxxx").append(NEW_LINE);
                        case HALF -> outputBuilder.append(dateFormat.format(dayOfMonth)).append(" => [Half a day vacation]").append(NEW_LINE);
                        case QUARTER -> outputBuilder.append(dateFormat.format(dayOfMonth)).append(" => [Quarter of a day vacation]").append(NEW_LINE);
                    }

                    numberOfWorkedDays += 1.0f - offday.amount().asDecimal();
                } else {
                    outputBuilder.append(dateFormat.format(dayOfMonth)).append(NEW_LINE);
                    numberOfWorkedDays++;
                }
            }
        }
        outputBuilder.append(NEW_LINE).append(String.format("Total of worked days in %s is %s",
                monthWithYear.getMonth().getDisplayName(TextStyle.FULL, Locale.getDefault()),
                formattedDecimalNumber(numberOfWorkedDays)
        ));

        this.monthWithYear = monthWithYear;
        this.offdays = offdays;
        this.numberOfWorkedDays = numberOfWorkedDays;
        this.stringOutput = outputBuilder.toString();
    }

    private String formattedDecimalNumber(float numberOfWorkedDays) {
        if (isARoundNumber(numberOfWorkedDays)) {
            return String.valueOf(Math.round(numberOfWorkedDays));
        } else {
            return new DecimalFormat("0.00").format(numberOfWorkedDays);
        }
    }

    public boolean isARoundNumber(float number) {
        return number == Math.round(number);
    }

    private boolean isWeekend(LocalDate dayOfMonth) {
        return dayOfMonth.getDayOfWeek() == DayOfWeek.SATURDAY || dayOfMonth.getDayOfWeek() == DayOfWeek.SUNDAY;
    }

    public List<Offday> offdays() {
        return offdays;
    }

    public float numberOfWorkedDays() {
        return numberOfWorkedDays;
    }

    public YearMonth monthWithYear() {
        return monthWithYear;
    }

    public String asString() {
        return stringOutput;
    }
}
