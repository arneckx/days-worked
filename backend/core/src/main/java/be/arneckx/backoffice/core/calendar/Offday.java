package be.arneckx.backoffice.core.calendar;

import java.time.YearMonth;

public record Offday(
        int day,
        YearMonth yearMonth,
        String description,
        OffdayType type,
        AmountOfDay amount
) {

    public Offday {
        if (!yearMonth.isValidDay(day)) throw new IllegalArgumentException();
    }
}
