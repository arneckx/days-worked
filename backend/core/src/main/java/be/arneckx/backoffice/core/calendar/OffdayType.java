package be.arneckx.backoffice.core.calendar;

public enum OffdayType {
    VACATION, OFFICIAL_HOLIDAY
}
