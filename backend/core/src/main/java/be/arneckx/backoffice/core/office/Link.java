package be.arneckx.backoffice.core.office;

public record Link(
        String name,
        String href,
        String initial,
        int order
) {
}
