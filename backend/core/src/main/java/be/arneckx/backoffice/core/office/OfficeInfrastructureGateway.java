package be.arneckx.backoffice.core.office;

import java.util.Collection;
import java.util.List;

public interface OfficeInfrastructureGateway {

    List<Link> findAllLinks();

    Collection<Shortcut> findAllShortcuts();

}
