package be.arneckx.backoffice.core.office;

import jakarta.enterprise.context.ApplicationScoped;

import java.util.Collection;
import java.util.List;

@ApplicationScoped
public class OfficeService {

    private final OfficeInfrastructureGateway infrastructureGateway;

    public OfficeService(OfficeInfrastructureGateway infrastructureGateway) {
        this.infrastructureGateway = infrastructureGateway;
    }

    public List<Link> findAllLinks() {
        return infrastructureGateway.findAllLinks();
    }

    public Collection<Shortcut> findAllShortcuts() {
        return infrastructureGateway.findAllShortcuts();
    }
}
