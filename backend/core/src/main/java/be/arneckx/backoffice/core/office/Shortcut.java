package be.arneckx.backoffice.core.office;

public record Shortcut(
        String name,
        String icon,
        String href,
        String backgroundColor,
        int order
) {
}
