package be.arneckx.backoffice.core.transaction;

public record Account(
        String accountNumber,
        String bic,
        String name,
        String address,
        Transaction.TransactionCategory defaultCategory
) {
}
