package be.arneckx.backoffice.core.transaction;

public record CategoryResult(Transaction.TransactionCategory category, double amount) {
}
