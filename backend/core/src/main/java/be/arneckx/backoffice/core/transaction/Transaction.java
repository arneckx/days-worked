package be.arneckx.backoffice.core.transaction;

import java.time.Instant;
import java.util.Optional;

public record Transaction(
        Id id,
        Instant timestamp,
        Account account,
        String description,
        TransactionType type,
        float amount,
        float balance,
        Notice notice,
        TransactionCategory category
) {

    public Optional<Account> maybeAccount() {
        return Optional.ofNullable(account);
    }

    public enum TransactionType {
        INCOMING,
        OUTGOING
    }

    public enum TransactionCategory {
        RESTAURANT,
        INSURANCE,
        SALARY,
        SOFTWARE,
        HARDWARE,
        CONFERENCE,
        HOME,
        CAR,
        SOCIAL_SECURITY,
        VAT,
        TAXES,
        BOOKKEEPER,
        COMPANY,
        NONE,
    }

    public record Id(String value) {

        public Id {
            if (value.length() < 8) {
                throw new IllegalArgumentException(
                        String.format(
                                "The id of a transaction is defined by following format O{yearOfTransaction}{transactionNumber} (e.g. 02022001) but was %s",
                                value
                        )
                );
            }
        }
    }

    public record Notice(String value, boolean structured) {

        public Notice {
            if (structured && value.length() != 20) {
                throw new IllegalArgumentException(
                        String.format(
                                "Invalid format of structured notice. Expected format is ***800/0227/15559*** But was %s",
                                value
                        )
                );
            }
        }
    }

    public Transaction changeCategory(TransactionCategory category) {
        return new Transaction(
                id,
                timestamp,
                account,
                description,
                type,
                amount,
                balance,
                notice,
                category
        );
    }
}
