package be.arneckx.backoffice.core.transaction;

import static be.arneckx.backoffice.core.BackofficeClock.EUROPE_BRUSSELS;
import be.arneckx.backoffice.core.transaction.Transaction.TransactionCategory;
import jakarta.enterprise.context.ApplicationScoped;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@ApplicationScoped
class TransactionCSVMapper {

    private static final String COMMA_DELIMITER = ";";
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    private final TransactionInfrastructureGateway infrastructureGateway;

    TransactionCSVMapper(TransactionInfrastructureGateway infrastructureGateway) {
        this.infrastructureGateway = infrastructureGateway;
    }

    public List<Transaction> transactionsFor(InputStream inputStream) {
        return read(inputStream)
                .stream()
                .skip(1)
                .map(this::toTransaction)
                .toList();
    }

    private Transaction toTransaction(CsvLine csvLine) {
        String statementNumber = csvLine.asString(CsvPosition.ID_INDEX);
        float amount = Math.abs(csvLine.asFloat(CsvPosition.AMOUNT_INDEX));
        float balance = csvLine.asFloat(CsvPosition.AMOUNT_BALANCE_INDEX);
        Account account = mapAccount(csvLine);
        TransactionCategory category = Optional.ofNullable(account)
                .flatMap(infrastructureGateway::findLatestTransactionFor)
                .map(Transaction::category)
                .orElse(TransactionCategory.NONE);
        return new Transaction(
                new Transaction.Id(String.format("%s-%s-%s", statementNumber, amount, balance).replace(".", "")),
                timestampFor(csvLine.asString(CsvPosition.TIMESTAMP_INDEX)),
                account,
                csvLine.asString(CsvPosition.DESCRIPTION_INDEX),
                typeFor(csvLine.asString(CsvPosition.CREDIT_INDEX), csvLine.asString(CsvPosition.DEBIT_INDEX)),
                amount,
                balance,
                noticeFor(csvLine.asString(CsvPosition.STRUCTURED_NOTICE_INDEX), csvLine.asString(CsvPosition.FREE_NOTICE_INDEX)),
                category
        );
    }

    private Account mapAccount(CsvLine csvLine) {
        return Optional.ofNullable(csvLine.asString(CsvPosition.CLIENT_ACCOUNT_NUMBER_INDEX))
                .map(presentNumber ->
                        new Account(
                                presentNumber.replace(" ", ""),
                                csvLine.asString(CsvPosition.CLIENT_BIC_NUMBER_INDEX),
                                csvLine.asString(CsvPosition.CLIENT_NAME_INDEX),
                                csvLine.asString(CsvPosition.CLIENT_ADDRESS_INDEX),
                                TransactionCategory.NONE
                        ))
                .orElse(null);
    }

    private Instant timestampFor(String date) {
        LocalDate localDate = LocalDate.parse(date, DATE_TIME_FORMATTER);
        return localDate.atStartOfDay(ZoneId.of(EUROPE_BRUSSELS)).toInstant();
    }

    private Transaction.Notice noticeFor(String structuredNotice, String freeNotice) {
        if (structuredNotice != null && !structuredNotice.isBlank()) {
            return new Transaction.Notice(structuredNotice, true);
        }
        return new Transaction.Notice(freeNotice, false);
    }

    private Transaction.TransactionType typeFor(String credit, String debit) {
        if (credit == null && debit == null) {
            throw new IllegalArgumentException("Should be credit or debit");
        }
        return Optional.ofNullable(credit)
                .map(isCredit -> Transaction.TransactionType.INCOMING)
                .orElse(Transaction.TransactionType.OUTGOING);
    }

    private List<CsvLine> read(InputStream stream) {
        List<CsvLine> records = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(stream))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(COMMA_DELIMITER);
                records.add(new CsvLine(Arrays.asList(values)));
            }
            return records;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static class CsvLine {

        private final List<String> values;

        public CsvLine(List<String> values) {
            this.values = values;
        }

        public String asString(CsvPosition position) {
            String value = values.get(position.index).trim();
            if (value.isBlank()) {
                return null;
            }
            return value;
        }

        public float asFloat(CsvPosition position) {
            return Float.parseFloat(values.get(position.index).replace(',', '.'));
        }
    }

    public enum CsvPosition {
        ID_INDEX(4),
        TIMESTAMP_INDEX(5),
        DESCRIPTION_INDEX(6),
        AMOUNT_INDEX(8),
        AMOUNT_BALANCE_INDEX(9),
        CREDIT_INDEX(10),
        DEBIT_INDEX(11),
        CLIENT_ACCOUNT_NUMBER_INDEX(12),
        CLIENT_BIC_NUMBER_INDEX(13),
        CLIENT_NAME_INDEX(14),
        CLIENT_ADDRESS_INDEX(15),
        STRUCTURED_NOTICE_INDEX(16),
        FREE_NOTICE_INDEX(17);

        private final int index;

        CsvPosition(int index) {
            this.index = index;
        }

        public int index() {
            return index;
        }
    }
}
