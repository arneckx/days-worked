package be.arneckx.backoffice.core.transaction;

import java.time.Year;
import java.util.List;
import java.util.Optional;

public interface TransactionInfrastructureGateway {

    void save(List<Transaction> transactions);

    List<Transaction> findAll(int page, int size);

    List<Transaction> findAllFor(Year year);

    void update(Transaction transaction);

    Optional<Transaction> findById(Transaction.Id transactionId);

    Optional<Transaction> findLatestTransactionFor(Account account);
}
