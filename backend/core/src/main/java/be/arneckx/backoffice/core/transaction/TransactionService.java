package be.arneckx.backoffice.core.transaction;

import jakarta.enterprise.context.ApplicationScoped;

import java.io.InputStream;
import java.time.Year;
import java.util.List;

@ApplicationScoped
public class TransactionService {

    private final TransactionCSVMapper transactionCSVMapper;
    private final TransactionInfrastructureGateway infrastructureGateway;

    public TransactionService(TransactionCSVMapper transactionCSVMapper, TransactionInfrastructureGateway infrastructureGateway) {
        this.transactionCSVMapper = transactionCSVMapper;
        this.infrastructureGateway = infrastructureGateway;
    }

    public void importFrom(InputStream inputStream) {
        List<Transaction> transactions = transactionCSVMapper.transactionsFor(inputStream);
        infrastructureGateway.save(transactions);
    }

    public void updateCategory(Transaction.Id transactionId, Transaction.TransactionCategory category) {
        Transaction existingTransaction = infrastructureGateway.findById(transactionId)
                .orElseThrow(() -> new RuntimeException(String.format("No transaction with id %s found", transactionId.value())));
        infrastructureGateway.update(existingTransaction.changeCategory(category));
    }

    public List<Transaction> findAll(int page, int size) {
        return infrastructureGateway.findAll(page, size);
    }

    public TransactionsYearOverview overviewFor(int year) {
        return new TransactionsYearOverview(infrastructureGateway.findAllFor(Year.of(year)));
    }
}
