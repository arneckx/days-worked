package be.arneckx.backoffice.core.transaction;

import java.util.List;
import java.util.stream.Collectors;

public class TransactionsYearOverview {

    private final int  amountOfTransactions;
    private final float incomingTransaction;
    private final float outgoingTransaction;
    private final List<CategoryResult> categories;

    private TransactionsYearOverview(int amountOfTransactions, float incomingTransaction, float outgoingTransaction, List<CategoryResult> categories) {
        this.amountOfTransactions = amountOfTransactions;
        this.incomingTransaction = incomingTransaction;
        this.outgoingTransaction = outgoingTransaction;
        this.categories = categories;
    }

    public TransactionsYearOverview(List<Transaction> transactions) {
        this(
                transactions.size(),
                (float) transactions.stream().filter(t -> t.type() == Transaction.TransactionType.INCOMING).mapToDouble(Transaction::amount).sum(),
                (float) transactions.stream().filter(t -> t.type() == Transaction.TransactionType.OUTGOING).mapToDouble(Transaction::amount).sum(),
                transactions.stream()
                        .filter(t -> t.type() == Transaction.TransactionType.OUTGOING)
                        .collect(Collectors.groupingBy(Transaction::category, Collectors.summingDouble(Transaction::amount)))
                        .entrySet()
                        .stream()
                        .map(entry -> new CategoryResult(entry.getKey(), entry.getValue()))
                        .toList()
        );
    }

    public int amountOfTransactions() {
        return amountOfTransactions;
    }

    public float incomingTransaction() {
        return incomingTransaction;
    }

    public float outgoingTransaction() {
        return outgoingTransaction;
    }

    public List<CategoryResult> categories() {
        return categories;
    }
}
