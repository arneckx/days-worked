package be.arneckx.backoffice.core.calendar;

import org.junit.jupiter.api.Test;

import java.time.Clock;
import java.time.Instant;
import java.time.YearMonth;
import java.time.ZoneId;
import java.util.List;

import static be.arneckx.backoffice.core.BackofficeClock.EUROPE_BRUSSELS;
import static org.assertj.core.api.Assertions.assertThat;

class CalendarYearOverviewTest {

    @Test
    void yearOverviewCalculatesYearStats() {
        List<Offday> offdays = List.of(
                offday(1, 1),
                offday(12, 31),
                offday(9, 4),
                offday(9, 5, AmountOfDay.QUARTER),
                offday(9, 6),
                offday(9, 7)
        );
        Clock clock = Clock.fixed(Instant.parse("2023-11-25T06:02:00.000Z"), ZoneId.of(EUROPE_BRUSSELS));
        CalendarYearOverview yearOverview = new CalendarYearOverview(2023, offdays, clock);

        assertThat(yearOverview.totalWorkedDays()).isEqualTo(231.75f);
        assertThat(yearOverview.expectedWorkedDays()).isEqualTo(256.75f);
        assertThat(yearOverview.totalOffdays()).isEqualTo(5.25f);
    }

    public Offday offday(int month, int day, AmountOfDay amountOfDay) {
        return new Offday(
                day,
                YearMonth.of(2023, month),
                "",
                OffdayType.VACATION,
                amountOfDay
        );
    }

    public Offday offday(int month, int day) {
        return offday(month, day, AmountOfDay.FULL);
    }
}