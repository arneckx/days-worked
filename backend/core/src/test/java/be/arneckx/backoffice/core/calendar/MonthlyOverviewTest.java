package be.arneckx.backoffice.core.calendar;

import org.junit.jupiter.api.Test;

import java.time.Month;
import java.time.YearMonth;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class MonthlyOverviewTest {

    @Test
    void workedDaysAugust2023() {
        MonthlyOverview workedDays = overviewFor(YearMonth.of(2023, Month.AUGUST), List.of(8, 9, 10, 11, 12, 15));

        String actualOutput = workedDays.asString();

        assertThat(actualOutput).isEqualTo("""
                2023-08-01 Tue
                2023-08-02 Wed
                2023-08-03 Thu
                2023-08-04 Fri
                                
                                
                2023-08-07 Mon
                xxxxxxxxxxxxxx
                xxxxxxxxxxxxxx
                xxxxxxxxxxxxxx
                xxxxxxxxxxxxxx
                                
                                
                2023-08-14 Mon
                xxxxxxxxxxxxxx
                2023-08-16 Wed
                2023-08-17 Thu
                2023-08-18 Fri
                                
                                
                2023-08-21 Mon
                2023-08-22 Tue
                2023-08-23 Wed
                2023-08-24 Thu
                2023-08-25 Fri
                                
                                
                2023-08-28 Mon
                2023-08-29 Tue
                2023-08-30 Wed
                2023-08-31 Thu
                                
                Total of worked days in August is 18""");
    }

    @Test
    void workedDaysOctober2023() {
        MonthlyOverview workedDays = overviewFor(YearMonth.of(2023, Month.OCTOBER), List.of(4, 5, 20));

        String actualOutput = workedDays.asString();

        assertThat(actualOutput).isEqualTo("""
                
                2023-10-02 Mon
                2023-10-03 Tue
                xxxxxxxxxxxxxx
                xxxxxxxxxxxxxx
                2023-10-06 Fri
                                
                                
                2023-10-09 Mon
                2023-10-10 Tue
                2023-10-11 Wed
                2023-10-12 Thu
                2023-10-13 Fri
                                
                                
                2023-10-16 Mon
                2023-10-17 Tue
                2023-10-18 Wed
                2023-10-19 Thu
                xxxxxxxxxxxxxx
                                
                                
                2023-10-23 Mon
                2023-10-24 Tue
                2023-10-25 Wed
                2023-10-26 Thu
                2023-10-27 Fri
                                
                                
                2023-10-30 Mon
                2023-10-31 Tue
                                
                Total of worked days in October is 19""");
    }

    @Test
    void workedDaysFebruary2023() {
        MonthlyOverview workedDays = overviewFor(YearMonth.of(2023, Month.FEBRUARY), List.of(6));

        String actualOutput = workedDays.asString();

        assertThat(actualOutput).isEqualTo("""
                2023-02-01 Wed
                2023-02-02 Thu
                2023-02-03 Fri
                
                
                xxxxxxxxxxxxxx
                2023-02-07 Tue
                2023-02-08 Wed
                2023-02-09 Thu
                2023-02-10 Fri
                
                
                2023-02-13 Mon
                2023-02-14 Tue
                2023-02-15 Wed
                2023-02-16 Thu
                2023-02-17 Fri
                
                
                2023-02-20 Mon
                2023-02-21 Tue
                2023-02-22 Wed
                2023-02-23 Thu
                2023-02-24 Fri
                
                
                2023-02-27 Mon
                2023-02-28 Tue
                                                
                Total of worked days in February is 19""");
    }

    @Test
    void halfOfADayAnQuarterOfADayAreDisplayedDifferently() {
        YearMonth yearMonth = YearMonth.of(2023, Month.OCTOBER);
        MonthlyOverview workedDays = new MonthlyOverview(
                yearMonth,
                List.of(
                        new Offday(
                                4,
                                yearMonth,
                                "",
                                OffdayType.VACATION,
                                AmountOfDay.HALF
                        ),
                        new Offday(
                                5,
                                yearMonth,
                                "",
                                OffdayType.VACATION,
                                AmountOfDay.QUARTER
                        )
                )
        );

        assertThat(workedDays.numberOfWorkedDays()).isEqualTo(21.25f);
        assertThat(workedDays.asString()).isEqualTo("""
                
                2023-10-02 Mon
                2023-10-03 Tue
                2023-10-04 Wed => [Half a day vacation]
                2023-10-05 Thu => [Quarter of a day vacation]
                2023-10-06 Fri
                                
                                
                2023-10-09 Mon
                2023-10-10 Tue
                2023-10-11 Wed
                2023-10-12 Thu
                2023-10-13 Fri
                                
                                
                2023-10-16 Mon
                2023-10-17 Tue
                2023-10-18 Wed
                2023-10-19 Thu
                2023-10-20 Fri
                                
                                
                2023-10-23 Mon
                2023-10-24 Tue
                2023-10-25 Wed
                2023-10-26 Thu
                2023-10-27 Fri
                                
                                
                2023-10-30 Mon
                2023-10-31 Tue
                                
                Total of worked days in October is 21,25""");
    }

    private MonthlyOverview overviewFor(YearMonth yearMonth, List<Integer> dayoffs) {
        return new MonthlyOverview(yearMonth, dayoffs.stream()
                .map(day -> new Offday(
                        day,
                        yearMonth,
                        "",
                        OffdayType.VACATION,
                        AmountOfDay.FULL
                ))
                .toList());
    }
}