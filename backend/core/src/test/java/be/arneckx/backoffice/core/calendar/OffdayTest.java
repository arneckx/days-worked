package be.arneckx.backoffice.core.calendar;

import org.junit.jupiter.api.Test;

import java.time.Month;
import java.time.YearMonth;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

class OffdayTest {

    @Test
    void invalidDayShouldThrowException() {
        Throwable throwable = catchThrowable(() -> new Offday(
                29,
                YearMonth.of(2023, Month.FEBRUARY),
                "",
                OffdayType.VACATION,
                AmountOfDay.FULL
        ));

        assertThat(throwable).isInstanceOf(IllegalArgumentException.class);
    }
}