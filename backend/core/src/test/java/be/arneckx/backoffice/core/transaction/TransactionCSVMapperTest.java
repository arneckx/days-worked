package be.arneckx.backoffice.core.transaction;

import org.assertj.core.api.Assertions;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.mock;

import java.io.IOException;
import java.io.InputStream;
import java.time.Instant;
import java.util.List;

class TransactionCSVMapperTest {

    private final TransactionInfrastructureGateway infrastructureGateway = mock(TransactionInfrastructureGateway.class);
    private final TransactionCSVMapper csvToTransaction = new TransactionCSVMapper(infrastructureGateway);

    @Test
    void mapCsvToTransactions() {
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        try (InputStream is = classloader.getResourceAsStream("export_test.csv")) {
            List<Transaction> transactions = csvToTransaction.transactionsFor(is);

            Assertions.assertThat(transactions).hasSize(9);
            assertThat(transactions.get(0)).isEqualTo(new Transaction(
                    new Transaction.Id("02022106-67176-1103725"),
                    Instant.parse("2022-12-30T23:00:00Z"),
                    new Account(
                            "BE92000066710000",
                            "KREDBEBBXXX",
                            "TEGENPARTIJ 1",
                            null,
                            Transaction.TransactionCategory.NONE
                    ),
                    "mijn omschrijving",
                    Transaction.TransactionType.OUTGOING,
                    671.76f,
                    11037.25f,
                    new Transaction.Notice("mededeling", false),
                    Transaction.TransactionCategory.NONE
            ));
            assertThat(transactions.get(3).type()).isEqualTo(Transaction.TransactionType.INCOMING);
            assertThat(transactions.get(5).notice().structured()).isTrue();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}