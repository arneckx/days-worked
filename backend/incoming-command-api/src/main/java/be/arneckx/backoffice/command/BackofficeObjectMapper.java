package be.arneckx.backoffice.command;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class BackofficeObjectMapper extends ObjectMapper {

    BackofficeObjectMapper() {
        super();
        configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        setDefaultPropertyInclusion(JsonInclude.Include.NON_NULL);
    }

    @Override
    public ObjectMapper copy() {
        return new BackofficeObjectMapper();
    }

    @Override
    public ObjectMapper copyWith(JsonFactory factory) {
        return new BackofficeObjectMapper();
    }
}
