package be.arneckx.backoffice.command;

public interface Command {

    Action action();

    Class<? extends Dto> supportedDto();

    Result handle(Dto dto);
}
