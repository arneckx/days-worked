package be.arneckx.backoffice.command;

import io.quarkus.arc.All;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@ApplicationScoped
class CommandFactory {

    private final Map<Action, Command> commands;

    CommandFactory(@All List<Command> commands) {
        this.commands = commands.stream().collect(Collectors.toMap(Command::action, Function.identity()));
    }

    public Command findFor(Action action) {
        return commands.get(action);
    }
}
