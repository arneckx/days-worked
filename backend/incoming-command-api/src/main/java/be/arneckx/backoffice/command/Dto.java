package be.arneckx.backoffice.command;

import java.io.InputStream;

public interface Dto {

    record None() implements Dto {}

    record Message(String message) implements Dto {}

    record InputStreamDto(InputStream inputStream) implements Dto {}

    record Page(int page, int size) implements Dto {}
}
