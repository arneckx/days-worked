package be.arneckx.backoffice.command;

import be.arneckx.backoffice.core.ThrowingFunction;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;

@ApplicationScoped
public class HttpCommandAdapter {

    private static final Logger LOGGER = LoggerFactory.getLogger(HttpCommandAdapter.class);
    public static final String HTTP_CUSTOM_COMMAND_HEADER = "X-Backoffice-Command";

    private final CommandFactory commandFactory;
    private final ObjectMapper mapper;

    @Inject
    HttpCommandAdapter(CommandFactory commandFactory, ObjectMapper mapper) {
        this.commandFactory = commandFactory;
        this.mapper = mapper;
    }

    public Response handle(String commandHeader, String requestBody) {
        return handle(commandHeader, (command) ->  mapper.readValue(requestBody, command.supportedDto()));
    }

    public Response handle(String commandHeader, InputStream inputStream) {
        return handle(commandHeader, (command) ->  new Dto.InputStreamDto(inputStream));
    }

    public Response handle(String commandHeader, ThrowingFunction<Command, Dto> dtoSupplier) {
        Action action = Action.valueOf(commandHeader);
        return withExceptionHandling(commandFactory.findFor(action), command -> {
            Dto dto = dtoSupplier.apply(command);
            Result result = command.handle(dto);
            return responseFor(result);
        });
    }

    private Response responseFor(Result result) {
        // TODO: [main] java21 use Pattern Matching for instanceof
        if (result instanceof Result.Success) {
            return Response.empty();
        }
        if (result instanceof Result.SuccessWithResult successWithResult) {
            return Response.withBody(successWithResult.result());
        }
        if (result instanceof Result.Error error) {
            return Response.error(error.reason());
        }
        throw new RuntimeException(String.format("result of type %s is not known", result.getClass().getSimpleName()));
    }

    private Response withExceptionHandling(Command command, ThrowingFunction<Command, Response> action) {
        try {
            return action.apply(command);
        } catch (JsonProcessingException jsonProcessingException) {
            LOGGER.error(jsonProcessingException.getMessage());
            return  Response.error("Something went wrong when parsing the json");
        } catch (Exception exception) {
            throw new RuntimeException(exception);
        }
    }
}
