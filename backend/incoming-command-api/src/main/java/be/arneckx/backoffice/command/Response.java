package be.arneckx.backoffice.command;

import com.fasterxml.jackson.annotation.JsonGetter;

public class Response {

    private final Object result;
    private final String errorReason;

     private Response(Object result, String errorReason) {
        this.result = result;
        this.errorReason = errorReason;
    }

    static Response error(String errorReason){
        return new Response(null, errorReason);
    }

    static Response withBody(Dto body){
        return new Response(body, null);
    }

    static Response empty(){
        return new Response(null, null);
    }

    @JsonGetter
    public Object result() {
        return result;
    }

    @JsonGetter
    public String errorReason() {
        return errorReason;
    }
}
