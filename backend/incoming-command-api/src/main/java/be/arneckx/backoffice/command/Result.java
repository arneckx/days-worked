package be.arneckx.backoffice.command;

public sealed interface Result {

    record Success() implements Result { }

    record SuccessWithResult(Dto result) implements Result { }

    record Error(String reason) implements Result {

        public static Error unsupportedType() {
            return new Error("Unsupported dto type");
        }
    }
}
