package be.arneckx.backoffice.command;

import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
class WelcomeCommand implements Command {

    @Override
    public Action action() {
        return Action.WELCOME;
    }

    @Override
    public Class<? extends Dto> supportedDto() {
        return Dto.None.class;
    }

    @Override
    public Result handle(Dto dto) {
        return new Result.SuccessWithResult(new Dto.Message("Lambda is hot and ready to mingle"));
    }
}
