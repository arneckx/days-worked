package be.arneckx.backoffice.command;

public record YearDto(int year) implements Dto {
}
