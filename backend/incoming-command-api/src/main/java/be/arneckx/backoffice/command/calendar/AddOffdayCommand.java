package be.arneckx.backoffice.command.calendar;

import be.arneckx.backoffice.command.Action;
import be.arneckx.backoffice.command.Command;
import be.arneckx.backoffice.command.Dto;
import be.arneckx.backoffice.command.Result;
import be.arneckx.backoffice.core.calendar.CalendarService;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
class AddOffdayCommand implements Command {

    private final CalendarService calendarService;

    AddOffdayCommand(CalendarService calendarService) {
        this.calendarService = calendarService;
    }

    @Override
    public Action action() {
        return Action.CALENDAR_ADD_DAYOFFS;
    }

    @Override
    public Class<? extends Dto> supportedDto() {
        return OffdayDtoList.class;
    }

    @Override
    public Result handle(Dto dto) {
        if (dto instanceof OffdayDtoList offdays) {
            offdays.forEach(offday -> calendarService.add(offday.toDomain()));
            return new Result.Success();
        }
        return Result.Error.unsupportedType();
    }
}
