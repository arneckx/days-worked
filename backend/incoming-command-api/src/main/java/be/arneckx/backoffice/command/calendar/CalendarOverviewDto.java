package be.arneckx.backoffice.command.calendar;

import be.arneckx.backoffice.command.Dto;
import be.arneckx.backoffice.core.calendar.MonthlyOverview;

import java.util.Comparator;
import java.util.List;

record CalendarOverviewDto(double totalNumberOfWorkedDays, List<MonthlyOverviewDto> months) implements Dto {

    public static CalendarOverviewDto from(List<MonthlyOverview> result) {
        return new CalendarOverviewDto(
                result.stream().mapToDouble(MonthlyOverview::numberOfWorkedDays).sum(),
                result.stream()
                        .map(MonthlyOverviewDto::from)
                        .sorted(Comparator.comparing(MonthlyOverviewDto::year).thenComparing(MonthlyOverviewDto::month))
                        .toList()
        );
    }

    record MonthlyOverviewDto(int year, int month, List<OffdayDto> offdays, float numberOfWorkedDays, String stringOutput) {
        public static MonthlyOverviewDto from(MonthlyOverview monthlyOverview) {
            return new MonthlyOverviewDto(
                    monthlyOverview.monthWithYear().getYear(),
                    monthlyOverview.monthWithYear().getMonthValue(),
                    monthlyOverview.offdays().stream().map(OffdayDto::from).toList(),
                    monthlyOverview.numberOfWorkedDays(),
                    monthlyOverview.asString()
            );
        }
    }
}
