package be.arneckx.backoffice.command.calendar;

import be.arneckx.backoffice.command.Dto;
import be.arneckx.backoffice.core.calendar.CalendarYearOverview;

public record CalenderStatsDto(float totalWorkedDays, float expectedWorkedDays, float totalOffdays) implements Dto {
    public static Dto from(CalendarYearOverview result) {
        return new CalenderStatsDto(
                result.totalWorkedDays(),
                result.expectedWorkedDays(),
                result.totalOffdays()
        );
    }
}
