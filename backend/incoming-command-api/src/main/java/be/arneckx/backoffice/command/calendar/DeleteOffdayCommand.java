package be.arneckx.backoffice.command.calendar;

import be.arneckx.backoffice.command.Action;
import be.arneckx.backoffice.command.Command;
import be.arneckx.backoffice.command.Dto;
import be.arneckx.backoffice.command.Result;
import be.arneckx.backoffice.core.calendar.CalendarService;
import jakarta.enterprise.context.ApplicationScoped;

import java.time.YearMonth;

@ApplicationScoped
class DeleteOffdayCommand implements Command {

    private final CalendarService calendarService;

    DeleteOffdayCommand(CalendarService calendarService) {
        this.calendarService = calendarService;
    }

    @Override
    public Action action() {
        return Action.CALENDAR_DELETE_DAYOFF;
    }

    @Override
    public Class<? extends Dto> supportedDto() {
        return OffdayDto.class;
    }

    @Override
    public Result handle(Dto dto) {
        if (dto instanceof OffdayDto offdayDto) {
            calendarService.delete(YearMonth.of(offdayDto.year(), offdayDto.month()), offdayDto.day());
            return new Result.Success();
        }
        return Result.Error.unsupportedType();
    }
}
