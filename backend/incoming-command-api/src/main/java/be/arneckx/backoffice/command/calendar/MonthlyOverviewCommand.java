package be.arneckx.backoffice.command.calendar;

import be.arneckx.backoffice.command.Action;
import be.arneckx.backoffice.command.Command;
import be.arneckx.backoffice.command.Dto;
import be.arneckx.backoffice.command.Result;
import be.arneckx.backoffice.core.calendar.CalendarService;
import be.arneckx.backoffice.core.calendar.MonthlyOverview;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
class MonthlyOverviewCommand implements Command {

    private final CalendarService calendarService;

    MonthlyOverviewCommand(CalendarService calendarService) {
        this.calendarService = calendarService;
    }

    @Override
    public Action action() {
        return Action.CALENDAR_MONTHLY_OVERVIEW;
    }

    @Override
    public Class<? extends Dto> supportedDto() {
        return YearMonthDtoList.class;
    }

    @Override
    public Result handle(Dto dto) {
        if (dto instanceof YearMonthDtoList yearMonthDtos) {
            List<MonthlyOverview> result = calendarService.findDaysOffFor(yearMonthDtos.stream().map(YearMonthDto::toDomain).collect(Collectors.toSet()));
            return new Result.SuccessWithResult(CalendarOverviewDto.from(result));
        }
        return Result.Error.unsupportedType();
    }
}
