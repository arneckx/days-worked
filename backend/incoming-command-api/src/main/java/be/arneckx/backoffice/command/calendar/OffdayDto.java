package be.arneckx.backoffice.command.calendar;

import be.arneckx.backoffice.command.Dto;
import be.arneckx.backoffice.core.calendar.AmountOfDay;
import be.arneckx.backoffice.core.calendar.Offday;
import be.arneckx.backoffice.core.calendar.OffdayType;

import java.time.YearMonth;

record OffdayDto(
        int day,
        int month,
        int year,
        String description,
        String type,
        String amount
) implements Dto {
    public static OffdayDto from(Offday offday) {
        return new OffdayDto(
                offday.day(),
                offday.yearMonth().getMonthValue(),
                offday.yearMonth().getYear(),
                offday.description(),
                offday.type().name(),
                offday.amount().name()
        );
    }

    public Offday toDomain() {
        return new Offday(
                day,
                YearMonth.of(year, month),
                description,
                OffdayType.valueOf(type),
                AmountOfDay.valueOf(amount)
        );
    }
}