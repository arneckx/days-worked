package be.arneckx.backoffice.command.calendar;

import java.time.YearMonth;

record YearMonthDto(int year, int month) {

    public YearMonth toDomain() {
        return YearMonth.of(year, month);
    }
}
