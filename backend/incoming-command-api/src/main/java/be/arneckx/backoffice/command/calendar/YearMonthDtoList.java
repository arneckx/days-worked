package be.arneckx.backoffice.command.calendar;

import be.arneckx.backoffice.command.Dto;

import java.util.ArrayList;

class YearMonthDtoList extends ArrayList<YearMonthDto> implements Dto { }
