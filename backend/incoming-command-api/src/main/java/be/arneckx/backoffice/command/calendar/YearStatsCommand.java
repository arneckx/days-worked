package be.arneckx.backoffice.command.calendar;

import be.arneckx.backoffice.command.Action;
import be.arneckx.backoffice.command.Command;
import be.arneckx.backoffice.command.Dto;
import be.arneckx.backoffice.command.Result;
import be.arneckx.backoffice.command.YearDto;
import be.arneckx.backoffice.core.calendar.CalendarService;
import be.arneckx.backoffice.core.calendar.CalendarYearOverview;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
class YearStatsCommand implements Command {

    private final CalendarService calendarService;

    YearStatsCommand(CalendarService calendarService) {
        this.calendarService = calendarService;
    }

    @Override
    public Action action() {
        return Action.CALENDAR_YEAR_STATS;
    }

    @Override
    public Class<? extends Dto> supportedDto() {
        return YearDto.class;
    }

    @Override
    public Result handle(Dto dto) {
        if (dto instanceof YearDto yearDto) {
            CalendarYearOverview result = calendarService.overviewFor(yearDto.year());
            return new Result.SuccessWithResult(CalenderStatsDto.from(result));
        }
        return Result.Error.unsupportedType();
    }
}
