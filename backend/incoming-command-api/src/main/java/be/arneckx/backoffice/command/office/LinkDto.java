package be.arneckx.backoffice.command.office;

import be.arneckx.backoffice.command.Dto;
import be.arneckx.backoffice.core.office.Link;

import java.util.List;

public record LinkDto(
        String name,
        String href,
        String initial,
        int order
) {

    public record LinkDtoList(List<LinkDto> links) implements Dto {
    }

    public static LinkDto from(Link link) {
        return new LinkDto(
                link.name(),
                link.href(),
                link.initial(),
                link.order()
        );
    }
}
