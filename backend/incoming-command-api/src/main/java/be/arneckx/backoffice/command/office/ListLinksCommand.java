package be.arneckx.backoffice.command.office;

import be.arneckx.backoffice.command.Action;
import be.arneckx.backoffice.command.Command;
import be.arneckx.backoffice.command.Dto;
import be.arneckx.backoffice.command.Result;
import be.arneckx.backoffice.core.office.OfficeService;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
class ListLinksCommand implements Command {

    private final OfficeService officeService;

    ListLinksCommand(OfficeService officeService) {
        this.officeService = officeService;
    }

    @Override
    public Action action() {
        return Action.OFFICE_LINKS;
    }

    @Override
    public Class<? extends Dto> supportedDto() {
        return Dto.None.class;
    }

    @Override
    public Result handle(Dto dto) {
        return new Result.SuccessWithResult(
                new LinkDto.LinkDtoList(
                        officeService.findAllLinks()
                                .stream()
                                .map(LinkDto::from)
                                .toList()
                )
        );
    }
}
