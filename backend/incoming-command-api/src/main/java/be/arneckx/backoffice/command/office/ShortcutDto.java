package be.arneckx.backoffice.command.office;

import be.arneckx.backoffice.command.Dto;
import be.arneckx.backoffice.core.office.Shortcut;

import java.util.List;

public record ShortcutDto(
        String name,
        String icon,
        String href,
        String bgColor,
        int order
) {

    public record ShortcutDtoList(List<ShortcutDto> links) implements Dto {
    }

    public static ShortcutDto from(Shortcut shortcut) {
        return new ShortcutDto(
                shortcut.name(),
                shortcut.icon(),
                shortcut.href(),
                shortcut.backgroundColor(),
                shortcut.order()
        );
    }
}
