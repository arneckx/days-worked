package be.arneckx.backoffice.command.transactions;

import be.arneckx.backoffice.command.Action;
import be.arneckx.backoffice.command.Command;
import be.arneckx.backoffice.command.Dto;
import be.arneckx.backoffice.command.Result;
import be.arneckx.backoffice.core.transaction.TransactionService;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
class ImportTransactionsCommand implements Command {

    private final TransactionService transactionService;

    ImportTransactionsCommand(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @Override
    public Action action() {
        return Action.TRANSACTIONS_IMPORT;
    }

    @Override
    public Class<? extends Dto> supportedDto() {
        return Dto.InputStreamDto.class;
    }

    @Override
    public Result handle(Dto dto) {
        if (dto instanceof Dto.InputStreamDto inputStreamDto) {
            transactionService.importFrom(inputStreamDto.inputStream());
            return new Result.Success();
        }
        return Result.Error.unsupportedType();
    }
}
