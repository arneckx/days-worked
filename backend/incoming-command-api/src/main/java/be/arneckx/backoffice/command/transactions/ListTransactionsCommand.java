package be.arneckx.backoffice.command.transactions;

import be.arneckx.backoffice.command.Action;
import be.arneckx.backoffice.command.Command;
import be.arneckx.backoffice.command.Dto;
import be.arneckx.backoffice.command.Result;
import be.arneckx.backoffice.core.transaction.TransactionService;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
class ListTransactionsCommand implements Command {

    private final TransactionService transactionService;

    ListTransactionsCommand(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @Override
    public Action action() {
        return Action.TRANSACTIONS_LIST;
    }

    @Override
    public Class<? extends Dto> supportedDto() {
        return Dto.Page.class;
    }

    @Override
    public Result handle(Dto dto) {
        if (dto instanceof Dto.Page page) {

            return new Result.SuccessWithResult(
                    new TransactionDto.TransactionDtoList(
                            transactionService.findAll(page.page(), page.size())
                                    .stream()
                                    .map(TransactionDto::from)
                                    .toList()
                    )
            );
        }
        return Result.Error.unsupportedType();
    }
}
