package be.arneckx.backoffice.command.transactions;

import be.arneckx.backoffice.command.Action;
import be.arneckx.backoffice.command.Command;
import be.arneckx.backoffice.command.Dto;
import be.arneckx.backoffice.command.Result;
import static be.arneckx.backoffice.core.transaction.Transaction.Id;
import be.arneckx.backoffice.core.transaction.Transaction.TransactionCategory;
import be.arneckx.backoffice.core.transaction.TransactionService;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
class TransactionChangeCategoryCommand implements Command {

    private final TransactionService transactionService;

    TransactionChangeCategoryCommand(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @Override
    public Action action() {
        return Action.TRANSACTIONS_CHANGE_CATEGORY;
    }

    @Override
    public Class<? extends Dto> supportedDto() {
        return TransactionChangeCategoryDto.class;
    }

    @Override
    public Result handle(Dto dto) {
        if (dto instanceof TransactionChangeCategoryDto changeCategoryDto) {
            transactionService.updateCategory(new Id(changeCategoryDto.transactionId()), TransactionCategory.valueOf(changeCategoryDto.transactionCategory()));
            return new Result.Success();
        }
        return Result.Error.unsupportedType();
    }
}
