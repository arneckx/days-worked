package be.arneckx.backoffice.command.transactions;

import be.arneckx.backoffice.command.Dto;

public record TransactionChangeCategoryDto(String transactionId, String transactionCategory) implements Dto {
}
