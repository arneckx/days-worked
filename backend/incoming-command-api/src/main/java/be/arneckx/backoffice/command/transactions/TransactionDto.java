package be.arneckx.backoffice.command.transactions;

import be.arneckx.backoffice.command.Dto;
import be.arneckx.backoffice.core.transaction.Account;
import be.arneckx.backoffice.core.transaction.Transaction;

import java.util.List;
import java.util.Optional;

public record TransactionDto(
        String transactionId,
        long timestamp,
        String accountNumber,
        String description,
        String type,
        float amount,
        float balance,
        String noticeValue,
        boolean noticeIsStructured,
        String category
) {

    public static TransactionDto from(Transaction transaction) {
        return new TransactionDto(
                transaction.id().value(),
                transaction.timestamp().getEpochSecond(),
                Optional.ofNullable(transaction.account()).map(Account::accountNumber).orElse(null),
                transaction.description(),
                transaction.type().name(),
                transaction.amount(),
                transaction.balance(),
                transaction.notice().value(),
                transaction.notice().structured(),
                transaction.category().name()
        );
    }

    public record TransactionDtoList(List<TransactionDto> transactions) implements Dto {}
}
