package be.arneckx.backoffice.command.transactions;

import be.arneckx.backoffice.command.Dto;
import be.arneckx.backoffice.core.transaction.TransactionsYearOverview;

import java.util.Comparator;
import java.util.List;

public record TransactionStatsDto(int amountOfTransactions, float incomingTransaction, float outgoingTransaction, List<CategoryResultDto> categories) implements Dto {

    public static Dto from(TransactionsYearOverview result) {
        return new TransactionStatsDto(
                result.amountOfTransactions(),
                result.incomingTransaction(),
                result.outgoingTransaction(),
                result.categories()
                        .stream()
                        .map(c -> new CategoryResultDto(c.category().name(), c.amount()))
                        .sorted(Comparator.comparingDouble(CategoryResultDto::amount).reversed())
                        .toList()
        );
    }

    public record CategoryResultDto(String category, double amount) {
    }
}
