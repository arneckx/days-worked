package be.arneckx.backoffice.command.transactions;

import be.arneckx.backoffice.command.Action;
import be.arneckx.backoffice.command.Command;
import be.arneckx.backoffice.command.Dto;
import be.arneckx.backoffice.command.Result;
import be.arneckx.backoffice.command.calendar.CalenderStatsDto;
import be.arneckx.backoffice.command.YearDto;
import be.arneckx.backoffice.core.calendar.CalendarYearOverview;
import be.arneckx.backoffice.core.transaction.TransactionService;
import be.arneckx.backoffice.core.transaction.TransactionsYearOverview;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
class YearStatsCommand implements Command {

    private final TransactionService transactionService;

    YearStatsCommand(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @Override
    public Action action() {
        return Action.TRANSACTIONS_YEAR_STATS;
    }

    @Override
    public Class<? extends Dto> supportedDto() {
        return YearDto.class;
    }

    @Override
    public Result handle(Dto dto) {
        if (dto instanceof YearDto yearDto) {
            TransactionsYearOverview result = transactionService.overviewFor(yearDto.year());
            return new Result.SuccessWithResult(TransactionStatsDto.from(result));
        }
        return Result.Error.unsupportedType();
    }
}
