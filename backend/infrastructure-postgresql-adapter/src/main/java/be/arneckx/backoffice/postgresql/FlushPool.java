package be.arneckx.backoffice.postgresql;

import io.agroal.api.AgroalDataSource;
import io.quarkus.runtime.StartupEvent;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.event.Observes;
import jakarta.inject.Inject;
import org.crac.Core;
import org.crac.Resource;

// https://github.com/quarkusio/quarkus/issues/31401
// SnapStart creates an idle transaction that is not flushed.
// The first time the lambda is hit the following error is thrown;
// Error trying to transactionRollback local transaction: This connection has been closed.
@ApplicationScoped
public class FlushPool implements Resource {

    @Inject
    AgroalDataSource dataSource;

    void onStart(@Observes StartupEvent ev) {
        Core.getGlobalContext().register(this);
    }

    @Override
    public void beforeCheckpoint(org.crac.Context<? extends Resource> context) throws Exception {
    }

    @Override
    public void afterRestore(org.crac.Context<? extends Resource> context) throws Exception {
        dataSource.flush(AgroalDataSource.FlushMode.IDLE);
    }
}