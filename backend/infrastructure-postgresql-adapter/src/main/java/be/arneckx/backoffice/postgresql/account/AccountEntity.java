package be.arneckx.backoffice.postgresql.account;

import be.arneckx.backoffice.core.transaction.Account;
import be.arneckx.backoffice.core.transaction.Transaction;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "account")
public class AccountEntity {

    @Id
    @GeneratedValue
    private Long id;
    private String accountNumber;
    private String bic;
    private String name;
    private String address;
    private String defaultCategory;

    public AccountEntity() {
    }

    public AccountEntity(Long id, String accountNumber, String bic, String name, String address, String defaultCategory) {
        this.id = id;
        this.accountNumber = accountNumber;
        this.bic = bic;
        this.name = name;
        this.address = address;
        this.defaultCategory = defaultCategory;
    }

    public static AccountEntity from(Account account) {
        return new AccountEntity(
                null,
                account.accountNumber(),
                account.bic(),
                account.name(),
                account.address(),
                account.defaultCategory().name()
        );
    }

    public Account toDomain() {
        return new Account(
                accountNumber,
                bic,
                name,
                address,
                Transaction.TransactionCategory.valueOf(defaultCategory)
        );
    }
}
