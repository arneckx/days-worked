package be.arneckx.backoffice.postgresql.account;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Parameters;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.Optional;

@ApplicationScoped
public class AccountRepository implements PanacheRepository<AccountEntity> {

    public Optional<AccountEntity> findByAccountNumber(String accountNumber) {
        Parameters parameters = new Parameters()
                .and("accountNumber", accountNumber);

        return find("accountNumber = :accountNumber", parameters).firstResultOptional();
    }
}
