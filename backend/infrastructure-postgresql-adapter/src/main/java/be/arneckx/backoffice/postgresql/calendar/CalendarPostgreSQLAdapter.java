package be.arneckx.backoffice.postgresql.calendar;

import be.arneckx.backoffice.core.calendar.CalendarInfrastructureGateway;
import be.arneckx.backoffice.core.calendar.Offday;
import io.quarkus.narayana.jta.runtime.TransactionConfiguration;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.YearMonth;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@ApplicationScoped
public class CalendarPostgreSQLAdapter implements CalendarInfrastructureGateway {

    private static final Logger LOGGER = LoggerFactory.getLogger(CalendarPostgreSQLAdapter.class);

    private final OffdayRepository repository;

    public CalendarPostgreSQLAdapter(OffdayRepository repository) {
        this.repository = repository;
    }

    @Override
    @Transactional
    @TransactionConfiguration(timeout = 30)
    public void add(Offday offday) {
        Optional<OffdayEntity> presentOffday = repository.findByYearAndMonthAndDay(offday.yearMonth().getYear(), offday.yearMonth().getMonthValue(), offday.day());
        if (presentOffday.isPresent()) {
            repository.persist(presentOffday.get().update(offday));
        } else {
            repository.persist(OffdayEntity.from(offday));
        }
    }

    @Override
    @Transactional
    @TransactionConfiguration(timeout = 30)
    public List<Offday> findDaysOffFor(Set<YearMonth> months) {
        long start = System.currentTimeMillis();
        List<Offday> result = months.stream()
                .flatMap(yearMonth -> repository.findAllByMonthAndYear(yearMonth.getMonth().getValue(), yearMonth.getYear()).stream())
                .map(OffdayEntity::toDomain)
                .toList();
        LOGGER.info("findDaysOffFor in {} ms", (System.currentTimeMillis() - start));
        return result;
    }

    @Override
    @Transactional
    @TransactionConfiguration(timeout = 30)
    public List<Offday> findDaysOffFor(int year) {
        long start = System.currentTimeMillis();
        List<Offday> result = repository.findAllByYear(year)
                .stream()
                .map(OffdayEntity::toDomain)
                .toList();
        LOGGER.info("findDaysOffFor in {} ms", (System.currentTimeMillis() - start));
        return result;
    }

    @Override
    @Transactional
    @TransactionConfiguration(timeout = 30)
    public void delete(YearMonth yearMonth, int day) {
        Optional<OffdayEntity> maybeOffday = repository.findByYearAndMonthAndDay(yearMonth.getYear(), yearMonth.getMonthValue(), day);
        maybeOffday.ifPresent(repository::delete);
    }
}
