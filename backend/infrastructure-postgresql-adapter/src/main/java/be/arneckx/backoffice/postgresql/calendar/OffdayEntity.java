package be.arneckx.backoffice.postgresql.calendar;

import be.arneckx.backoffice.core.calendar.AmountOfDay;
import be.arneckx.backoffice.core.calendar.Offday;
import be.arneckx.backoffice.core.calendar.OffdayType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import java.time.YearMonth;

@Entity
@Table(name = "offday")
public class OffdayEntity {

    @Id
    @GeneratedValue
    private Long id;
    private int day;
    private int month;
    private int year;
    private String description;
    private String type;
    private float amount;

    public OffdayEntity() {
    }

    public OffdayEntity(Long id, int day, int month, int year, String description, String type, float amount) {
        this.id = id;
        this.day = day;
        this.month = month;
        this.year = year;
        this.description = description;
        this.type = type;
        this.amount = amount;
    }

    public static OffdayEntity from(Offday offday) {
        return new OffdayEntity(
                null,
                offday.day(),
                offday.yearMonth().getMonth().getValue(),
                offday.yearMonth().getYear(),
                offday.description(),
                offday.type().name(),
                offday.amount().asDecimal()
        );
    }

    public Offday toDomain() {
        return new Offday(
                day,
                YearMonth.of(year, month),
                description,
                OffdayType.valueOf(type),
                AmountOfDay.of(amount)
        );
    }

    public OffdayEntity update(Offday offday) {
        description = offday.description();
        type = offday.type().name();
        amount = offday.amount().asDecimal();
        return this;
    }
}
