package be.arneckx.backoffice.postgresql.calendar;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Parameters;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.List;
import java.util.Optional;

@ApplicationScoped
public class OffdayRepository implements PanacheRepository<OffdayEntity> {

    public List<OffdayEntity> findAllByMonthAndYear(int month, int year) {
        Parameters parameters = new Parameters()
                .and("month", month)
                .and("year", year);

        return list("month = :month and year = :year" , parameters);
    }

    public Optional<OffdayEntity> findByYearAndMonthAndDay(int year, int month, int day) {
        Parameters parameters = new Parameters()
                .and("month", month)
                .and("year", year)
                .and("day", day);

        return find("month = :month and year = :year and day = :day", parameters).firstResultOptional();
    }

    public List<OffdayEntity> findAllByYear(int year) {
        Parameters parameters = new Parameters()
                .and("year", year);

        return list("year = :year" , parameters);
    }
}
