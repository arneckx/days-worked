package be.arneckx.backoffice.postgresql.office;

import be.arneckx.backoffice.core.office.Link;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "link")
public class LinkEntity {

    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private String href;
    private String initial;
    private int linkorder;

    public LinkEntity() {
    }

    public LinkEntity(Long id, String name, String href, String initial, int linkorder) {
        this.id = id;
        this.linkorder = linkorder;
        this.name = name;
        this.href = href;
        this.initial = initial;
    }

    public Link toDomain() {
        return new Link(
                name,
                href,
                initial,
                linkorder
        );
    }
}