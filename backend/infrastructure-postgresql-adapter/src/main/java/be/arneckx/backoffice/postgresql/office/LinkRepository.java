package be.arneckx.backoffice.postgresql.office;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class LinkRepository implements PanacheRepository<LinkEntity> {
}
