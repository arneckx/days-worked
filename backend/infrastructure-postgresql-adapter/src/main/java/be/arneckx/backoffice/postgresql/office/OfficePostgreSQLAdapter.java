package be.arneckx.backoffice.postgresql.office;

import be.arneckx.backoffice.core.office.Link;
import be.arneckx.backoffice.core.office.OfficeInfrastructureGateway;
import be.arneckx.backoffice.core.office.Shortcut;
import io.quarkus.narayana.jta.runtime.TransactionConfiguration;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.transaction.Transactional;

import java.util.List;

@ApplicationScoped
public class OfficePostgreSQLAdapter implements OfficeInfrastructureGateway {

    private final LinkRepository linkRepository;
    private final ShortcutRepository shortcutRepository;

    public OfficePostgreSQLAdapter(LinkRepository linkRepository, ShortcutRepository shortcutRepository) {
        this.linkRepository = linkRepository;
        this.shortcutRepository = shortcutRepository;
    }

    @Override
    @Transactional
    @TransactionConfiguration(timeout = 30)
    public List<Link> findAllLinks() {
        return linkRepository.findAll()
                .stream()
                .map(LinkEntity::toDomain)
                .toList();
    }

    @Override
    @Transactional
    @TransactionConfiguration(timeout = 30)
    public List<Shortcut> findAllShortcuts() {
        return shortcutRepository.findAll()
                .stream()
                .map(ShortcutEntity::toDomain)
                .toList();
    }
}
