package be.arneckx.backoffice.postgresql.office;

import be.arneckx.backoffice.core.office.Shortcut;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "shortcut")
public class ShortcutEntity {

    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private String icon;
    private String href;
    private String backgroundColor;
    private int shortcutorder;

    public ShortcutEntity() {
    }

    public ShortcutEntity(Long id, String name, String icon, String href, String backgroundColor, int shortcutorder) {
        this.id = id;
        this.name = name;
        this.icon = icon;
        this.href = href;
        this.backgroundColor = backgroundColor;
        this.shortcutorder = shortcutorder;
    }

    public Shortcut toDomain() {
        return new Shortcut(
                name,
                icon,
                href,
                backgroundColor,
                shortcutorder
        );
    }
}