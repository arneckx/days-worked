package be.arneckx.backoffice.postgresql.transaction;

import be.arneckx.backoffice.core.transaction.Account;
import be.arneckx.backoffice.core.transaction.Transaction;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import java.time.Instant;
import java.util.Optional;

@Entity
@Table(name = "transaction")
public class TransactionEntity {

    @Id
    @GeneratedValue
    private Long id;
    private String transactionId;
    private Instant timestamp;
    private String accountNumber;
    private String description;
    private String type;
    private float amount;
    private float balance;
    private String noticeValue;
    private boolean noticeIsStructured;
    private String category;

    public TransactionEntity() {
    }

    public TransactionEntity(Long id, String transactionId, Instant timestamp, String accountNumber, String description, String type, float amount, float balance, String noticeValue, boolean noticeIsStructured, String category) {
        this.id = id;
        this.transactionId = transactionId;
        this.timestamp = timestamp;
        this.accountNumber = accountNumber;
        this.description = description;
        this.type = type;
        this.amount = amount;
        this.balance = balance;
        this.noticeValue = noticeValue;
        this.noticeIsStructured = noticeIsStructured;
        this.category = category;
    }

    public static TransactionEntity from(Transaction transaction) {
        return new TransactionEntity(
                null,
                transaction.id().value(),
                transaction.timestamp(),
                Optional.ofNullable(transaction.account()).map(Account::accountNumber).orElse(null),
                transaction.description(),
                transaction.type().name(),
                transaction.amount(),
                transaction.balance(),
                transaction.notice().value(),
                transaction.notice().structured(),
                transaction.category().name()
        );
    }

    public Transaction toDomain(Account account) {
        return new Transaction(
                new Transaction.Id(transactionId),
                timestamp,
                account,
                description,
                Transaction.TransactionType.valueOf(type),
                amount,
                balance,
                new Transaction.Notice(
                        noticeValue,
                        noticeIsStructured
                ),
                Transaction.TransactionCategory.valueOf(category)
        );
    }

    public Long id() {
        return id;
    }

    public String accountNumber() {
        return accountNumber;
    }

    public TransactionEntity update(Transaction transaction) {
        category = transaction.category().name();
        return this;
    }
}