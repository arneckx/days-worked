package be.arneckx.backoffice.postgresql.transaction;

import be.arneckx.backoffice.core.transaction.Account;
import be.arneckx.backoffice.core.transaction.Transaction;
import be.arneckx.backoffice.core.transaction.TransactionInfrastructureGateway;
import be.arneckx.backoffice.postgresql.account.AccountEntity;
import be.arneckx.backoffice.postgresql.account.AccountRepository;
import io.quarkus.narayana.jta.runtime.TransactionConfiguration;
import io.quarkus.panache.common.Page;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Year;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

@ApplicationScoped
public class TransactionPostgreSQLAdapter implements TransactionInfrastructureGateway {

    private static final Logger LOGGER = LoggerFactory.getLogger(TransactionPostgreSQLAdapter.class);

    private final TransactionRepository transactionRepository;
    private final AccountRepository accountRepository;

    public TransactionPostgreSQLAdapter(TransactionRepository transactionRepository, AccountRepository accountRepository) {
        this.transactionRepository = transactionRepository;
        this.accountRepository = accountRepository;
    }

    @Override
    @Transactional
    @TransactionConfiguration(timeout = 30)
    public void save(List<Transaction> transactions) {
        transactions.forEach(transaction ->
                transactionRepository.findByTransactionId(transaction.id())
                        .ifPresentOrElse(
                                presentTransaction -> LOGGER.info("Doing nothing. Transaction with id already exists {}", transaction.id().value()),
                                () -> createTransaction(transaction)
                        ));
    }

    private void createTransaction(Transaction transaction) {
        LOGGER.info("Creating transaction with id {}", transaction.id().value());
        if (transaction.maybeAccount().isPresent() &&
                accountRepository.findByAccountNumber(transaction.account().accountNumber()).isEmpty()) {
            accountRepository.persist(AccountEntity.from(transaction.account()));
        }
        transactionRepository.persist(TransactionEntity.from(transaction));
    }

    @Override
    @Transactional
    @TransactionConfiguration(timeout = 30)
    public List<Transaction> findAll(int page, int size) {
        return transactionRepository.findFor(Page.of(page, size))
                .stream()
                .map(toDomain())
                .toList();
    }

    @Override
    @Transactional
    @TransactionConfiguration(timeout = 30)
    public List<Transaction> findAllFor(Year year) {
        return transactionRepository.findByYear(year)
                .stream()
                .map(toDomain())
                .toList();
    }

    @Override
    @Transactional
    @TransactionConfiguration(timeout = 30)
    public void update(Transaction transaction) {
        TransactionEntity existingTransaction = transactionRepository.findByTransactionId(transaction.id()).orElseThrow();
        transactionRepository.persist(existingTransaction.update(transaction));
    }

    @Override
    @Transactional
    @TransactionConfiguration(timeout = 30)
    public Optional<Transaction> findById(Transaction.Id transactionId) {
        return transactionRepository.findByTransactionId(transactionId)
                .map(toDomain());
    }

    @Override
    @Transactional
    @TransactionConfiguration(timeout = 30)
    public Optional<Transaction> findLatestTransactionFor(Account account) {
        return transactionRepository.findLatestByAccountNumber(account.accountNumber())
                .map(toDomain());
    }

    private Function<TransactionEntity, Transaction> toDomain() {
        return transaction -> transaction.toDomain(
                Optional.ofNullable(transaction.accountNumber())
                        .map(accountRepository::findByAccountNumber)
                        .filter(Optional::isPresent)
                        .map(existingAccount -> existingAccount.get().toDomain())
                        .orElse(null)
        );
    }
}
