package be.arneckx.backoffice.postgresql.transaction;

import static be.arneckx.backoffice.core.BackofficeClock.EUROPE_BRUSSELS;
import be.arneckx.backoffice.core.transaction.Transaction;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Page;
import io.quarkus.panache.common.Parameters;
import io.quarkus.panache.common.Sort;
import jakarta.enterprise.context.ApplicationScoped;

import java.time.MonthDay;
import java.time.Year;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;

@ApplicationScoped
public class TransactionRepository implements PanacheRepository<TransactionEntity> {

    public Optional<TransactionEntity> findByTransactionId(Transaction.Id id) {
        Parameters parameters = new Parameters()
                .and("transactionId", id.value());

        return find("transactionId = :transactionId order by timestamp", parameters).firstResultOptional();
    }

    public List<TransactionEntity> findByYear(Year year) {
        Parameters parameters = new Parameters()
                .and("before", year.plusYears(1).atMonthDay(MonthDay.of(1, 1)).atStartOfDay(ZoneId.of(EUROPE_BRUSSELS)).toInstant())
                .and("after", year.atMonthDay(MonthDay.of(1, 1)).atStartOfDay(ZoneId.of(EUROPE_BRUSSELS)).toInstant());

        return list("timestamp >= :after and timestamp <= :before order by timestamp", parameters).stream().toList();
    }

    public List<TransactionEntity> findFor(Page page) {
        return findAll(Sort.descending("timestamp")).page(page).stream().toList();
    }

    public Optional<TransactionEntity> findLatestByAccountNumber(String accountNumber) {
        Parameters parameters = new Parameters()
                .and("accountNumber", accountNumber);
        return find("accountNumber = :accountNumber order by timestamp desc", parameters).firstResultOptional();
    }
}
