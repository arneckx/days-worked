create sequence offday_seq
    increment by 50;


create table offday (
                        amount      real    not null,
                        day         integer not null,
                        month       integer not null,
                        year        integer not null,
                        id          bigint  not null
                            primary key,
                        description varchar(255),
                        type        varchar(255)
);

alter table offday
    owner to postgres;

CREATE INDEX index_name ON offday (year, month);