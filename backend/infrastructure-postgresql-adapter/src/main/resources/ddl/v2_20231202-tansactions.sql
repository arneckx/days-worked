create sequence account_seq
    increment by 50;

create sequence transaction_seq
    increment by 50;

create table account (
                         id              bigint not null
                             primary key,
                         accountnumber   varchar(255),
                         address         varchar(255),
                         bic             varchar(255),
                         defaultcategory varchar(255),
                         name            varchar(255)
);

create table transaction (
                             id                 bigint  not null
                                 primary key,
                             accountnumber      varchar(255),
                             amount             real    not null,
                             balance            real    not null,
                             category           varchar(255),
                             description        varchar(510),
                             noticeisstructured boolean not null,
                             noticevalue        varchar(510),
                             timestamp          timestamp(6) with time zone,
                             transactionid      varchar(255),
                             type               varchar(255)
);

CREATE INDEX account_accountnumber_index ON account (accountnumber);
CREATE INDEX transaction_transactionid_index ON transaction (transactionid);