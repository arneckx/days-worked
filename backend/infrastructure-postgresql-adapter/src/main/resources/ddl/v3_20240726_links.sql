create sequence link_seq increment by 50;


create table link (
                        linkorder         integer not null,
                        id          bigint not null primary key,
                        name        varchar(255),
                        href        varchar(255),
                        initial     varchar(10)
);

alter table link
    owner to postgres;