create sequence shortcut_seq increment by 50;


create table shortcut (
    id              bigint  not null primary key,
    name            varchar(255),
    icon            varchar(50),
    href            varchar(255),
    backgroundColor varchar(20),
    shortcutorder   integer not null
);
