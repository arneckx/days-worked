package be.arneckx.backoffice.postgresql.calendar;

import be.arneckx.backoffice.core.calendar.AmountOfDay;
import be.arneckx.backoffice.core.calendar.Offday;
import be.arneckx.backoffice.core.calendar.OffdayType;
import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Month;
import java.time.YearMonth;
import java.util.List;
import java.util.Set;

import static java.time.Month.*;
import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
class CalendarPostgreSQLAdapterTest {

    @Inject
    private CalendarPostgreSQLAdapter adapter;
    @Inject
    private OffdayRepository repository;

    @BeforeEach
    @Transactional
    void setUp() {
        repository.deleteAll();
    }

    @AfterEach
    @Transactional
    void tearDown() {
        repository.deleteAll();
    }

    @Test
    void addSomeOffdays() {
        Offday offday1 = offday(1, SEPTEMBER, 2023);
        Offday offday2 = offday(4, SEPTEMBER, 2023);
        adapter.add(offday1);
        adapter.add(offday2);

        List<Offday> daysOff = adapter.findDaysOffFor(Set.of(YearMonth.of(2023, SEPTEMBER)));

        assertThat(daysOff)
                .hasSize(2)
                .containsExactlyInAnyOrder(offday1, offday2);
    }

    @Test
    void sameOffdayIsUpdated() {
        Offday offday1 = offday(4, SEPTEMBER, 2023, "First description");
        Offday offday2 = offday(4, SEPTEMBER, 2023, "Second description");
        adapter.add(offday1);
        adapter.add(offday2);

        List<Offday> daysOff = adapter.findDaysOffFor(Set.of(YearMonth.of(2023, SEPTEMBER)));

        assertThat(daysOff).hasSize(1);
        assertThat(daysOff.get(0).description()).isEqualTo("Second description");
    }

    @Test
    void deleteOffday() {
        Offday offday1 = offday(1, SEPTEMBER, 2023);
        Offday offday2 = offday(4, SEPTEMBER, 2023);
        adapter.add(offday1);
        adapter.add(offday2);

        adapter.delete(YearMonth.of(2023, SEPTEMBER), 1);

        List<Offday> daysOff = adapter.findDaysOffFor(Set.of(YearMonth.of(2023, SEPTEMBER)));
        assertThat(daysOff)
                .hasSize(1)
                .containsExactlyInAnyOrder(offday2);
    }

    @Test
    void allOffdaysOfTheYearAreFetched() {
        Offday offday1 = offday(1, DECEMBER, 2023);
        Offday offday2 = offday(4, JANUARY, 2023);
        Offday offday3 = offday(4, SEPTEMBER, 2024);
        adapter.add(offday1);
        adapter.add(offday2);
        adapter.add(offday3);

        List<Offday> daysOff = adapter.findDaysOffFor(2023);
        assertThat(daysOff)
                .hasSize(2)
                .containsExactlyInAnyOrder(offday1, offday2);
    }

    private Offday offday(int day, Month month, int year, String description) {
        return new Offday(
                day,
                YearMonth.of(year, month.getValue()),
                description,
                OffdayType.VACATION,
                AmountOfDay.FULL
        );
    }

    private Offday offday(int day, Month month, int year) {
        return offday(
                day,
                month,
                year,
                ""
        );
    }
}