package be.arneckx.backoffice.postgresql.transaction;

import static be.arneckx.backoffice.core.BackofficeClock.EUROPE_BRUSSELS;
import be.arneckx.backoffice.core.transaction.Account;
import be.arneckx.backoffice.core.transaction.Transaction;
import be.arneckx.backoffice.postgresql.account.AccountRepository;
import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Clock;
import java.time.Instant;
import java.time.Year;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;

@QuarkusTest
class TransactionPostgreSQLAdapterTest {

    @Inject
    private TransactionPostgreSQLAdapter adapter;
    @Inject
    private TransactionRepository transactionRepository;
    @Inject
    private AccountRepository accountRepository;

    private final Clock clock2023 = Clock.fixed(Instant.parse("2023-11-25T06:02:00.000Z"), ZoneId.of(EUROPE_BRUSSELS));
    private final Clock clock2022 = Clock.fixed(Instant.parse("2022-11-25T06:02:00.000Z"), ZoneId.of(EUROPE_BRUSSELS));
    private final Clock clock2024 = Clock.fixed(Instant.parse("2024-11-25T06:02:00.000Z"), ZoneId.of(EUROPE_BRUSSELS));

    @BeforeEach
    @Transactional
    void setUp() {
        accountRepository.deleteAll();
        transactionRepository.deleteAll();
    }

    @AfterEach
    @Transactional
    void tearDown() {
        accountRepository.deleteAll();
        transactionRepository.deleteAll();
    }

    @Test
    public void existingAccountsAndTransactionsAreNotUpdated() {
        adapter.save(List.of(
                transaction(new Transaction.Id("02022001")),
                transaction(new Transaction.Id("02022002")),
                transaction(new Transaction.Id("02022001"))
        ));

        assertThat(transactionRepository.findAll().stream().count()).isEqualTo(2L);
        assertThat(accountRepository.findAll().stream().count()).isEqualTo(1L);
    }

    @Test
    void findTransactionsByYear() {
        Clock clock2023FirstMoment = Clock.fixed(Instant.parse("2023-01-01T00:00:00.000Z"), ZoneId.of(EUROPE_BRUSSELS));
        Clock clock2023LastMoment = Clock.fixed(Instant.parse("2023-12-31T23:00:00.000Z"), ZoneId.of(EUROPE_BRUSSELS));
        adapter.save(List.of(
                transaction(new Transaction.Id("02022001"), clock2023.instant()),
                transaction(new Transaction.Id("02022002"), clock2022.instant()),
                transaction(new Transaction.Id("02022003"), clock2024.instant()),
                transaction(new Transaction.Id("02022004"), clock2023FirstMoment.instant()),
                transaction(new Transaction.Id("02022005"), clock2023LastMoment.instant())
        ));

        List<Transaction> result = adapter.findAllFor(Year.of(2023));
        assertThat(result)
                .extracting(Transaction::id)
                .containsExactly(
                        new Transaction.Id("02022004"),
                        new Transaction.Id("02022001"),
                        new Transaction.Id("02022005")
                );
    }

    @Test
    void updateTransaction() {
        Transaction transaction = transaction(new Transaction.Id("02022001"));
        adapter.save(List.of(transaction));

        Transaction updatedTransaction = transaction.changeCategory(Transaction.TransactionCategory.SOFTWARE);
        adapter.update(updatedTransaction);

        Optional<Transaction> result = adapter.findById(transaction.id());
        assertThat(result).hasValueSatisfying(presentValue ->
                assertThat(presentValue.category()).isEqualTo(Transaction.TransactionCategory.SOFTWARE));
    }

    @Test
    void latestTransactionByAccountNumberIsFound() {
        String accountNumber = "BE123456789";
        adapter.save(List.of(
                transaction(new Transaction.Id("02022001"), clock2023.instant()).changeCategory(Transaction.TransactionCategory.HARDWARE),
                transaction(new Transaction.Id("02022002"), clock2022.instant()).changeCategory(Transaction.TransactionCategory.SOFTWARE),
                transaction(new Transaction.Id("02022003"), clock2024.instant()).changeCategory(Transaction.TransactionCategory.HOME)
        ));

        Optional<Transaction> result = adapter.findLatestTransactionFor(new Account(accountNumber, "", "", "", Transaction.TransactionCategory.NONE));

        assertThat(result).hasValueSatisfying(presentValue ->
                assertThat(presentValue.category()).isEqualTo(Transaction.TransactionCategory.HOME));
    }

    private Transaction transaction(Transaction.Id id, Instant timestamp) {
        return new Transaction(
                id,
                timestamp,
                new Account(
                        "BE123456789",
                        "BIC",
                        "name",
                        "address",
                        Transaction.TransactionCategory.HARDWARE
                ),
                "description",
                Transaction.TransactionType.OUTGOING,
                12.3f,
                4590.0f,
                new Transaction.Notice(
                        "Payment",
                        false
                ),
                Transaction.TransactionCategory.HARDWARE
        );
    }

    private Transaction transaction(Transaction.Id id) {
        return transaction(id, Instant.now());
    }
}