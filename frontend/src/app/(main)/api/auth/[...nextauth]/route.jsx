import NextAuth from "next-auth";
import CognitoProvider from "next-auth/providers/cognito";

export const authOptions = {
  // TODO: [main] localhost keycloak client
  providers: [
    CognitoProvider({
      clientId: process.env.COGNITO_CLIENT_ID,
      clientSecret: process.env.COGNITO_CLIENT_SECRET,
      issuer: process.env.COGNITO_ISSUER,
      idToken: true,
    }),
  ],
  theme: {
    colorScheme: "dark", // "auto" | "dark" | "light"
    brandColor: "#004F76", // Hex color code
    logo: "https://www.arneckx.be/_next/image?url=%2Flogo-white.png&w=3840&q=75", // Absolute URL to image
    buttonText: "#fff", // Hex color code
  },
  callbacks: {
    async jwt({ token, account }) {
      if (account) {
        token.idToken = account?.id_token;
      }
      return token;
    },
    async session({ session, token }) {
      session.idToken = token?.idToken;
      return session;
    },
  },
  jwt: {
    maxAge: 60 * 60, // 1 hour
  },
  session: {
    maxAge: 60 * 60, // 1 hour
  },
};
const handler = NextAuth(authOptions);

export { handler as GET, handler as POST };
