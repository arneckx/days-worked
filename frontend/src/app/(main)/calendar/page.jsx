"use client";

import {ChevronLeftIcon, ChevronRightIcon} from "@heroicons/react/20/solid";
import {useContext, useEffect, useState} from "react";
import YearMonth from "@/src/lib/calendar/YearMonth";
import {classNames} from "@/src/lib/css-util";
import Day from "@/src/components/calendar/Day";
import OffdayDetail from "@/src/components/calendar/OddayDetail";
import {ClipboardDocumentIcon} from "@heroicons/react/24/outline";
import {BackofficeGatewayContext} from "@/src/context/BackofficeGatewayProvider";

class SelectedDay {
  open;
  day;

  constructor(open, day) {
    this.open = open;
    this.day = day;
  }
}

export default function Calendar() {
  let currentYearMonth = YearMonth.ofToday();
  const [data, setData] = useState();
  const [upcomingEvents, setUpcomingEvents] = useState([]);
  const [months, setMonths] = useState([
    currentYearMonth,
    currentYearMonth.nextMonth(),
  ]);
  const [selectedDay, setSelectedDay] = useState(new SelectedDay(false));
  const backofficeGateway = useContext(BackofficeGatewayContext);

  useEffect(() => {
    const fetchData = async () =>
      await backofficeGateway.calendar().overviewFor(months).then(setData);
    fetchData().catch(console.error);
  }, [backofficeGateway, months]);

  useEffect(() => {
    if (upcomingEvents.length === 0 && data) {
      setUpcomingEvents(
        Array.from(data.months.values()).flatMap((month) =>
          Array.from(month.offdays.values()).sort((a, b) =>
            `${a.year}${a.month}${a.day}`.localeCompare(
              `${b.year}${b.month}${b.day}`,
            ),
          ),
        ),
      );
    }
  }, [data, months, upcomingEvents.length]);

  return (
    <div>
      {
        new OffdayDetail(selectedDay, () =>
          setSelectedDay(new SelectedDay(false)),
        )
      }
      <div className="relative grid grid-cols-1 gap-x-14 md:grid-cols-2">
        <button
          type="button"
          className="absolute -top-1 flex items-center justify-center text-gray-400 -left-1.5 p-1.5 hover:text-gray-500"
          onClick={() => {
            setMonths([months[0].previousMonth(), months[0]]);
          }}
        >
          <span className="sr-only">Previous month</span>
          <ChevronLeftIcon className="h-5 w-5" aria-hidden="true" />
        </button>
        <button
          type="button"
          className="absolute -top-1 flex items-center justify-center text-gray-400 -right-1.5 p-1.5 hover:text-gray-500"
          onClick={() => {
            setMonths([months[1], months[1].nextMonth()]);
          }}
        >
          <span className="sr-only">Next month</span>
          <ChevronRightIcon className="h-5 w-5" aria-hidden="true" />
        </button>
        {months
          .map((month) => month.allDays(data))
          .map((month, monthIdx) => (
            <section
              key={monthIdx}
              className={classNames(
                monthIdx === months.length - 1 && "hidden md:block",
                "text-center",
              )}
            >
              <h2 className="text-sm font-semibold text-gray-900">
                {month.name}
                <span
                  className="ml-1 inline-flex cursor-pointer items-center rounded-md bg-lime-200 text-xs font-medium text-green-800 px-1.5 py-0.5"
                  onClick={() => {
                    navigator.clipboard.writeText(month.stringOutput);
                  }}
                >
                  {month.numberOfWorkedDays ? month.numberOfWorkedDays : "?"}{" "}
                  working days
                  <span>
                    <ClipboardDocumentIcon className="ml-1 inline-flex h-4 w-4" />
                  </span>
                </span>
              </h2>

              <div className="mt-6 grid grid-cols-7 text-xs leading-6 text-gray-500">
                <div>M</div>
                <div>T</div>
                <div>W</div>
                <div>T</div>
                <div>F</div>
                <div>S</div>
                <div>S</div>
              </div>
              <div className="isolate mt-2 grid grid-cols-7 gap-px rounded-lg bg-gray-200 text-sm shadow ring-1 ring-gray-200">
                {month.days.map(
                  (day, dayIndex) =>
                    new Day(day, dayIndex, month, () =>
                      setSelectedDay(new SelectedDay(true, day)),
                    ),
                )}
              </div>
            </section>
          ))}
      </div>
      <section className="mt-12">
        <h2 className="text-base font-semibold leading-6 text-gray-900">
          Upcoming events
        </h2>
        <ol className="mt-2 text-sm leading-6 text-gray-500 divide-y divide-gray-200">
          {upcomingEvents.length === 0 && (
            <li className="py-4 sm:flex">
              <p className="mt-2 flex-auto sm:mt-0">No upcoming events</p>
            </li>
          )}
          {upcomingEvents.map((event) => (
            <li
              key={`upcoming-${event.day}-${event.month}-${event.year}`}
              className="py-4 sm:flex"
            >
              <time dateTime="2022-01-19" className="w-28 flex-none">
                {event.displayDate()}
              </time>
              <p className="mt-2 flex-auto font-semibold text-gray-900 sm:mt-0">
                {event.displayDescription()}
              </p>
              <p className="flex-none sm:ml-6">
                {event.displayAmountAndType()}
              </p>
            </li>
          ))}
        </ol>
      </section>
    </div>
  );
}
