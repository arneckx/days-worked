export default function Invoices() {
  return (
      <div className="px-4 sm:px-6 lg:px-8">
          <div className="sm:flex sm:items-center">
              <div className="sm:flex-auto">
                  <h1 className="text-base font-semibold leading-6 text-gray-900">
                      Invoices
                  </h1>
              </div>
          </div>
      </div>
  );
}
