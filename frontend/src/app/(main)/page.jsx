"use client";

import {useContext, useEffect, useState} from "react";
import {BackofficeGatewayContext} from "@/src/context/BackofficeGatewayProvider";
import Loading from "@/src/components/atoms/Loading";
import CalendarStats from "@/src/components/home/CalendarStats";
import TransactionStats from "@/src/components/home/TransactionStats";
import {ArrowUpRightIcon, ChevronLeftIcon, ChevronRightIcon} from "@heroicons/react/20/solid";
import {classNames} from "@/src/lib/css-util";
import {findIcon} from "@/src/lib/heroicons-util";

export default function Hello() {
    const [data, setData] = useState();
    const [shortcuts, setShortcuts] = useState();
    const [year, setYear] = useState(new Date().getFullYear());
    const backofficeGateway = useContext(BackofficeGatewayContext);

    useEffect(() => {
        const fetchData = async () =>
            await backofficeGateway.office().shortcuts().then(setShortcuts);
        fetchData().catch(console.error);
    }, [backofficeGateway]);

    useEffect(() => {
        const fetchData = async () => {
            await backofficeGateway.welcome().then((data) => {
                setData(data?.result?.message);
            });
        };
        fetchData().catch(console.error);
    }, [backofficeGateway, setData]);

    return (
        <div>
            <h1 className="text-4xl font-bold tracking-tight text-pink-500 sm:text-6xl">
                Welcome Arne!
            </h1>
            <p className="mt-6 text-lg leading-8 text-gray-700">Nice to see you ✨</p>
            <div>
                <h2 className="text-sm font-medium text-gray-500">Shortcuts</h2>
                <ul role="list" className="mt-3 grid grid-cols-1 gap-5 sm:grid-cols-2 sm:gap-6 lg:grid-cols-4">
                    {shortcuts?.map((shortcut) => (
                        <li key={shortcut.name} className="col-span-1 flex rounded-md shadow-sm">
                            <div
                                className={classNames(
                                    shortcut.bgColor,
                                    'flex w-16 flex-shrink-0 items-center justify-center rounded-l-md text-sm font-medium text-white',
                                )}
                            >
                                {findIcon(shortcut.icon)}
                            </div>
                            <div className="flex flex-1 items-center justify-between truncate rounded-r-md border-t border-r border-b border-gray-200 bg-white">
                                <a href={shortcut.href} target="_blank" className="font-medium text-gray-900 hover:text-gray-600">
                                    <div className="flex-1 truncate px-4 py-2 text-sm">
                                        {shortcut.name}
                                    </div>
                                </a>
                                <div className="flex-shrink-0 pr-2">
                                    <a
                                        href={shortcut.href} target="_blank"
                                        type="button"
                                        className="inline-flex h-8 w-8 items-center justify-center rounded-full bg-transparent bg-white text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                                    >
                                        <span className="sr-only">Open shortcut</span>
                                        <ArrowUpRightIcon aria-hidden="true" className="h-5 w-5"/>
                                    </a>
                                </div>
                            </div>
                        </li>
                    ))}
                </ul>
            </div>
            {!data ? (
                <p className="mt-6 inline-block text-lg leading-8 text-gray-700">
                    <Loading/>
                    Waiting for the lambda to warm up.
                </p>
            ) : (
                <div>
                    <p className="mt-6 text-lg leading-8 text-gray-700">{data}</p>
                    <div className="relative mt-6 flex items-center rounded-md md:items-stretch">
                        <button
                            type="button"
                            className="flex h-9 w-12 items-center justify-center rounded-l-md border-y border-l border-gray-300 pr-1 text-gray-400 hover:text-pink-700 focus:relative md:w-9 md:pr-0"
                            onClick={() => {
                                setYear(year - 1);
                            }}
                        >
                            <span className="sr-only">Previous year</span>
                            <ChevronLeftIcon className="h-5 w-5" aria-hidden="true"/>
                        </button>
                        <button
                            type="button"
                            className="border-y border-gray-300 text-sm font-semibold text-gray-900 px-3.5 focus:relative"
                        >
                            {year}
                        </button>
                        <span className="relative -mx-px h-5 w-px bg-gray-300 md:hidden"/>
                        <button
                            type="button"
                            className="flex h-9 w-12 items-center justify-center rounded-r-md border-y border-r border-gray-300 pl-1 text-gray-400 hover:text-pink-700 focus:relative md:w-9 md:pl-0"
                            onClick={() => {
                                setYear(year + 1);
                            }}
                        >
                            <span className="sr-only">Next year</span>
                            <ChevronRightIcon className="h-5 w-5" aria-hidden="true"/>
                        </button>
                    </div>
                    <div className="isolate mx-auto mt-3 grid max-w-md grid-cols-1 gap-8 lg:mx-0 lg:max-w-none lg:grid-cols-3">
                        <CalendarStats year={year}/>
                        <TransactionStats year={year}/>
                    </div>
                    <div id="colors" hidden>
                        <span className="bg-blue-600"></span>
                        <span className="bg-green-500"></span>
                        <span className="bg-green-500"></span>
                        <span className="bg-yellow-500"></span>
                        <span className="bg-blue-900"></span>
                        <span className="bg-pink-600"></span>
                        <span className="bg-blue-900"></span>
                    </div>
                </div>
            )}
        </div>
    );
}
