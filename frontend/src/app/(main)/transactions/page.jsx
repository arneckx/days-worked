"use client";

import ImportTransactions from "@/src/components/transactions/ImportTransactions";
import {useCallback, useContext, useEffect, useRef, useState} from "react";
import {BackofficeGatewayContext} from "@/src/context/BackofficeGatewayProvider";
import TransactionRecord from "@/src/components/transactions/TransactionRecord";
import Loading from "@/src/components/atoms/Loading";

export default function Transactions() {
  const pageSize = 25;
  const [loading, setLoading] = useState(false);
  const [transactions, setTransactions] = useState([]);
  const bottom = useRef(null);
  const [page, setPage] = useState(0);
  const [finalTransactionReached, setFinalTransactionReached] = useState(false);
  const backofficeGateway = useContext(BackofficeGatewayContext);

  const fetchData = useCallback(async () => {
    if (loading || finalTransactionReached) return;
    setLoading(true);
    try {
      await backofficeGateway
        .transactions()
        .findAll(page, pageSize)
        .then((data) => {
          data.length < pageSize && setFinalTransactionReached(true);
          setTransactions((prev) => [...prev, ...data]);
        });
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }

    setPage(page + 1);
  }, [page, loading, finalTransactionReached]);

  useEffect(() => {
    const observer = new IntersectionObserver((entries) => {
      const target = entries[0];
      if (target.isIntersecting) {
        fetchData().catch(console.error);
      }
    });

    if (bottom.current) {
      observer.observe(bottom.current);
    }

    return () => {
      if (bottom.current) {
        observer.unobserve(bottom.current);
      }
    };
  }, [fetchData]);

  useEffect(() => {
    fetchData().catch(console.error);
  }, []);

  return (
    <div className="px-4 sm:px-6 lg:px-8">
      <div className="sm:flex sm:items-center">
        <div className="sm:flex-auto">
          <h1 className="text-base font-semibold leading-6 text-gray-900">
            Transactions
          </h1>
        </div>
        <ImportTransactions />
      </div>

      <div className="mt-8 flow-root">
        <div className="-mx-4 -my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
          <div className="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8">
            <table className="min-w-full table-auto divide-y divide-gray-300">
              <thead>
                <tr>
                  <th
                    scope="col"
                    className="whitespace-nowrap px-2 text-left text-sm font-semibold text-gray-900 py-3.5"
                  >
                    Timestamp
                  </th>
                  <th
                    scope="col"
                    className="whitespace-nowrap px-2 text-left text-sm font-semibold text-gray-900 py-3.5"
                  >
                    Company
                  </th>
                  <th
                    scope="col"
                    className="block whitespace-nowrap px-2 text-left text-sm font-semibold text-gray-900 py-3.5"
                  >
                    Description
                  </th>
                  <th
                    scope="col"
                    className="whitespace-nowrap px-2 text-left text-sm font-semibold text-gray-900 py-3.5"
                  >
                    Type
                  </th>
                  <th
                    scope="col"
                    className="whitespace-nowrap px-2 text-left text-sm font-semibold text-gray-900 py-3.5"
                  >
                    Amount
                  </th>
                  <th
                    scope="col"
                    className="whitespace-nowrap px-2 text-left text-sm font-semibold text-gray-900 py-3.5"
                  >
                    Notice
                  </th>
                  <th
                    scope="col"
                    className="whitespace-nowrap px-2 text-left text-sm font-semibold text-gray-900 py-3.5"
                  >
                    Category
                  </th>
                  <th
                    scope="col"
                    className="relative whitespace-nowrap pr-4 pl-3 py-3.5 sm:pr-0"
                  >
                    <span className="sr-only">Edit</span>
                  </th>
                </tr>
              </thead>
              <tbody className="bg-gray-200 divide-y divide-gray-200">
                {transactions.map((transaction) => (
                  <TransactionRecord
                    key={transaction.transactionId}
                    transaction={transaction}
                  />
                ))}
              </tbody>
            </table>
            {loading && <Loading />}
            <div ref={bottom} />
          </div>
        </div>
      </div>
    </div>
  );
}
