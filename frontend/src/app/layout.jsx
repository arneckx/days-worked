import SessionProvider from "@/src/context/SessionProvider";
import "./globals.css";
import {getServerSession} from "next-auth";
import BackofficeGatewayProvider from "@/src/context/BackofficeGatewayProvider";
import {authOptions} from "@/src/app/(main)/api/auth/[...nextauth]/route";

export default async function RootLayout({ children }) {
  const session = await getServerSession(authOptions);
  return (
    <html lang="nl" className="h-full bg-gray-100">
      <head>
        <link
          rel="preconnect"
          href="https://cdn.fontshare.com"
          crossOrigin="anonymous"
        />
        <link
          rel="stylesheet"
          href="https://api.fontshare.com/v2/css?f[]=satoshi@700,500,400&display=swap"
        />
      </head>
      <body className="h-ful">
        <BackofficeGatewayProvider token={session.idToken}>
          <SessionProvider session={session}>{children}</SessionProvider>
        </BackofficeGatewayProvider>
      </body>
    </html>
  );
}
