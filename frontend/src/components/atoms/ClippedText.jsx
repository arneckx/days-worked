"use client";

import {useState} from "react";
import {classNames} from "@/src/lib/css-util";

export default function ClippedText({ text }) {
  let [isExpanded, setIsExpanded] = useState(!text || text.length <= 22);

  return (
    <div>
      <p className={classNames(!isExpanded && "line-clamp-1")}>{text}</p>
      {!isExpanded && (
        <button
          type="button"
          className="inline-block text-xs leading-6 text-pink-300 hover:text-pink-700 active:text-pink-900"
          onClick={() => setIsExpanded(true)}
        >
          Show more
        </button>
      )}
    </div>
  );
}
