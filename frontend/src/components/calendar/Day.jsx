import {classNames} from "@/src/lib/css-util";

export default function Day(day, dayIndex, month, clickAction) {
  const dayOfWeek = new Date(Date.parse(day.date)).getDay();
  const isWeekend = dayOfWeek === 6 || dayOfWeek === 0;
  return (
    <button
      key={day.date}
      type="button"
      className={classNames(
        day.isCurrentMonth && !isWeekend
          ? "bg-white text-gray-900 "
          : "bg-gray-50 text-gray-400  ",
        dayIndex === 0 && "rounded-tl-lg",
        dayIndex === 6 && "rounded-tr-lg",
        dayIndex === month.days.length - 7 && "rounded-bl-lg",
        dayIndex === month.days.length - 1 && "rounded-br-lg",
        "relative py-1.5 hover:bg-gray-100 focus:z-10",
      )}
      onClick={() => clickAction(day)}
    >
      <div className={"mx-auto flex "}>
        <time
          dateTime={day.date}
          className={classNames(
            day.isToday && "bg-blue-600 font-semibold text-white",
            "mx-auto flex h-7 w-7 items-center justify-center rounded-full",
          )}
        >
          {day.date.split("-").pop().replace(/^0/, "")}
        </time>
        {day.offday?.description && (
          <div className={"absolute ml-3"}>
            <svg
              className="fill-pink-500 h-1.5 w-1.5"
              viewBox="0 0 6 6"
              aria-hidden="true"
            >
              <circle cx={3} cy={3} r={3} />
            </svg>
          </div>
        )}
      </div>
    </button>
  );
}
