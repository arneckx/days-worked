"use client";

import {Fragment, useContext, useState} from "react";
import {Dialog, Transition} from "@headlessui/react";
import {XMarkIcon} from "@heroicons/react/24/outline";
import {OffdayDto} from "@/src/lib/calendar/CalendarOverviewDto";
import {BackofficeGatewayContext} from "@/src/context/BackofficeGatewayProvider";

const amountOptions = ["Full day", "Half a day", "Quarter of a day"];
const typeOptions = ["Official holiday", "Vacation"];

export default function OffdayDetail(selectedDay, closeDetail) {
  const backofficeGateway = useContext(BackofficeGatewayContext);

  const [description, setDescription] = useState(
    selectedDay.day && selectedDay.day.offday
      ? selectedDay.day.offday.description
      : "",
  );
  const [amount, setAmount] = useState(
    selectedDay.day && selectedDay.day.offday
      ? selectedDay.day.offday.displayAmount()
      : "",
  );
  const [type, setType] = useState(
    selectedDay.day && selectedDay.day.offday
      ? selectedDay.day.offday.displayType()
      : "",
  );

  let handleSubmit = async (e) => {
    e.preventDefault();
    let date = new Date(Date.parse(selectedDay.day.date));
    backofficeGateway
      .calendar()
      .addOffday(
        date.getDate(),
        date.getMonth() + 1,
        date.getFullYear(),
        description,
        OffdayDto.typeOf(type),
        OffdayDto.amountOf(amount),
      );
    closeDetail();
  };

  let handleDelete = async (e) => {
    let date = new Date(Date.parse(selectedDay.day.date));
    backofficeGateway
      .calendar()
      .deleteOffday(date.getDate(), date.getMonth() + 1, date.getFullYear());
    closeDetail();
  };

  return (
    <Transition.Root show={selectedDay.open} as={Fragment}>
      <Dialog as="div" className="relative z-50" onClose={closeDetail}>
        <div className="fixed inset-0" />

        <div className="fixed inset-0 overflow-hidden">
          <div className="absolute inset-0 overflow-hidden">
            <div className="pointer-events-none fixed inset-y-0 right-0 flex max-w-full pl-10 sm:pl-16">
              <Transition.Child
                as={Fragment}
                enter="transform transition ease-in-out duration-200 sm:duration-300"
                enterFrom="translate-x-full"
                enterTo="translate-x-0"
                leave="transform transition ease-in-out duration-100 sm:duration-200"
                leaveFrom="translate-x-0"
                leaveTo="translate-x-full"
              >
                <Dialog.Panel className="pointer-events-auto w-screen max-w-md">
                  <form
                    className="flex h-full flex-col bg-white shadow-xl divide-y divide-gray-200"
                    onSubmit={handleSubmit}
                  >
                    <div className="h-0 flex-1 overflow-y-auto">
                      <div className="bg-gray-900 px-4 py-6 sm:px-6">
                        <div className="flex items-center justify-between">
                          <Dialog.Title className="text-base font-semibold leading-6 text-white">
                            {selectedDay.day &&
                              selectedDay.day?.dateAsObject?.toLocaleString(
                                "en-BE",
                                {
                                  timeZone: "Europe/Brussels",
                                  weekday: "short",
                                  month: "short",
                                  day: "numeric",
                                },
                              )}
                          </Dialog.Title>
                          <div className="ml-3 flex h-7 items-center">
                            <button
                              type="button"
                              className="relative rounded-md bg-blue-700 text-blue-200 hover:text-white focus:outline-none focus:ring-2 focus:ring-white"
                              onClick={closeDetail}
                            >
                              <span className="absolute -inset-2.5" />
                              <span className="sr-only">Close panel</span>
                              <XMarkIcon
                                className="h-6 w-6"
                                aria-hidden="true"
                              />
                            </button>
                          </div>
                        </div>
                        <div className="mt-1">
                          <p className="text-sm text-gray-300">
                            Fill in the details for an offday.
                          </p>
                        </div>
                      </div>
                      <div className="flex flex-1 flex-col justify-between">
                        <div className="px-4 divide-y divide-gray-200 sm:px-6">
                          <div className="pt-6 pb-5 space-y-6">
                            <div>
                              <label
                                htmlFor="project-name"
                                className="block text-sm font-medium leading-6 text-gray-900"
                              >
                                Description
                              </label>
                              <div className="mt-2">
                                <input
                                  type="text"
                                  name="project-name"
                                  id="project-name"
                                  className="block w-full rounded-md border-0 placeholder:text-gray-400 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 py-1.5 focus:ring-2 focus:ring-inset focus:ring-blue-600 sm:text-sm sm:leading-6"
                                  defaultValue={
                                    selectedDay.day && selectedDay.day.offday
                                      ? selectedDay.day.offday.description
                                      : ""
                                  }
                                  onChange={(e) =>
                                    setDescription(e.target.value)
                                  }
                                />
                              </div>
                            </div>
                            <fieldset>
                              <legend className="text-sm font-medium leading-6 text-gray-900">
                                Amount
                              </legend>
                              <div className="mt-2 space-y-4">
                                {amountOptions.map((option) => (
                                  <div key={option.trim()}>
                                    <div className="relative flex items-start">
                                      <div className="absolute flex h-6 items-center">
                                        <input
                                          id={option.trim()}
                                          name="amount"
                                          type="radio"
                                          className="h-4 w-4 border-gray-300 text-pink-500 focus:ring-pink-600"
                                          defaultChecked={
                                            selectedDay.day &&
                                            selectedDay.day.offday &&
                                            selectedDay.day.offday.displayAmount() ===
                                              option
                                          }
                                          value={option}
                                          onChange={(e) =>
                                            setAmount(e.target.value)
                                          }
                                        />
                                      </div>
                                      <div className="pl-7 text-sm leading-6">
                                        <label
                                          htmlFor={option.trim()}
                                          className="font-medium text-gray-900"
                                        >
                                          {option}
                                        </label>
                                      </div>
                                    </div>
                                  </div>
                                ))}
                              </div>
                            </fieldset>
                            <fieldset>
                              <legend className="text-sm font-medium leading-6 text-gray-900">
                                Type
                              </legend>
                              <div className="mt-2 space-y-4">
                                {typeOptions.map((option) => (
                                  <div key={option.trim()}>
                                    <div className="relative flex items-start">
                                      <div className="absolute flex h-6 items-center">
                                        <input
                                          id={option.trim()}
                                          name="type"
                                          type="radio"
                                          className="h-4 w-4 border-gray-300 text-pink-500 focus:ring-pink-600"
                                          defaultChecked={
                                            selectedDay.day &&
                                            selectedDay.day.offday &&
                                            selectedDay.day.offday.displayType() ===
                                              option
                                          }
                                          value={option}
                                          onChange={(e) =>
                                            setType(e.target.value)
                                          }
                                        />
                                      </div>
                                      <div className="pl-7 text-sm leading-6">
                                        <label
                                          htmlFor={option.trim()}
                                          className="font-medium text-gray-900"
                                        >
                                          {option}
                                        </label>
                                      </div>
                                    </div>
                                  </div>
                                ))}
                              </div>
                            </fieldset>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="flex flex-shrink-0 justify-end px-4 py-4">
                      <button
                        type="submit"
                        className="inline-flex justify-center rounded-md bg-blue-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-blue-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-blue-600"
                      >
                        Add
                      </button>
                      <button
                        type="button"
                        className="ml-4 rounded-md bg-white px-3 py-2 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-gray-50"
                        onClick={closeDetail}
                      >
                        Cancel
                      </button>
                      <button
                        type="button"
                        className="ml-4 justify-self-start rounded-md bg-red-700 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-red-800 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-red-800"
                        onClick={handleDelete}
                      >
                        Delete
                      </button>
                    </div>
                  </form>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </div>
      </Dialog>
    </Transition.Root>
  );
}
