"use client";

import Chart from 'chart.js/auto'
import {useEffect, useRef} from "react";
import colors from 'tailwindcss/colors'

export default function CategoriesChart({categories}) {
    const chartRef = useRef(null);
    const chartInstance = useRef(null);

    useEffect(() => {
        if (chartInstance.current) {
            chartInstance.current.destroy();
        }
        if (categories) {
            const ctx = chartRef.current.getContext('2d');
            chartInstance.current = new Chart(ctx, {
                type: 'pie',
                options: {
                    responsive: true,
                    plugins: {
                        legend: {
                            position: 'bottom',
                        },
                    }
                },
                data: {
                    labels: categories.map(c => c.category),
                    datasets: [{
                        data: categories.map(c => c.amount),
                        backgroundColor: [
                            colors.green[500],
                            colors.yellow[500],
                            colors.blue[500],
                            colors.pink[500],
                            colors.red[500],
                            colors.green[900],
                            colors.yellow[900],
                            colors.blue[900],
                            colors.pink[900],
                            colors.red[900],
                            colors.green[200],
                            colors.yellow[200],
                            colors.blue[200],
                            colors.pink[200],
                            colors.red[200],
                        ],
                        hoverOffset: 32
                    }]
                }
            });
        }
    }, [categories]);

    return (
        <div key={"/chart"} className={"mt-4"}>
            <h3
                id={"/"}
                className={"leading-2 text-lg font-semibold text-gray-900 mb-4"}
            >
                Explore costs
            </h3>
            <p className="flex items-baseline gap-x-1 text-sm">
                <canvas ref={chartRef}/>
            </p>
        </div>
    );
}
