"use client";

import Link from "next/link";
import {BackofficeGatewayContext} from "@/src/context/BackofficeGatewayProvider";
import {useContext, useEffect, useState} from "react";
import Loading from "@/src/components/atoms/Loading";
import CategoriesChart from "@/src/components/home/CategoriesChart";

export default function TransactionStats({year}) {
  const [stats, setStats] = useState();
  const backofficeGateway = useContext(BackofficeGatewayContext);

  useEffect(() => {
    const fetchData = async () =>
      await backofficeGateway.transactions().stats(year).then(setStats);
    fetchData().catch(console.error);
  }, [backofficeGateway, year]);

  return (
    <div key={"/"} className={"rounded-3xl p-8 ring-1 ring-gray-200 xl:p-10"}>
      <div className="flex items-center justify-between gap-x-4">
        <h3
          id={"/"}
          className={"leading-2 text-lg font-semibold text-gray-900"}
        >
          Transaction stats
        </h3>
      </div>
      <p className="mt-6 flex items-baseline gap-x-1">
        <span className="text-4xl font-bold tracking-tight text-gray-900">
          {!stats ? <Loading /> : stats.amountOfTransactions}
        </span>
        <span className="text-sm font-semibold leading-6 text-gray-600">
           transactions
        </span>
      </p>
      <p className="mt-3 flex items-baseline gap-x-1">
        <span className="text-sm font-bold tracking-tight text-gray-900">
          {stats && stats.incomingTransaction}
        </span>
        <span className="text-sm font-semibold leading-6 text-gray-600">
          incoming
        </span>
      </p>
      <p className="flex items-baseline gap-x-1">
        <span className="text-sm font-bold tracking-tight text-gray-900">
          {stats && stats.outgoingTransaction}
        </span>
        <span className="text-sm font-semibold leading-6 text-gray-600">
          outgoing
        </span>
      </p>
      <Link
        href={"/transactions"}
        className={
          "mt-6 block rounded-md px-3 py-2 text-center text-sm font-semibold leading-6 text-pink-500 ring-1 ring-inset ring-pink-200 hover:ring-pink-300 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-pink-600"
        }
      >
        Go to transactions
      </Link>
        {stats && <CategoriesChart categories={stats.categories}/>}
    </div>
  );
}
