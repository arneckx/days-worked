"use client";

import {BackofficeGatewayContext} from "@/src/context/BackofficeGatewayProvider";
import {useContext} from "react";
import {CloudArrowUpIcon} from "@heroicons/react/24/outline";

export default function ImportTransactions() {
  const backofficeGateway = useContext(BackofficeGatewayContext);

  let handleSubmit = async (e) => {
    e.preventDefault();
    backofficeGateway
      .transactions()
      .importTransactions(document.getElementById("fileInput"));
  };

  return (
    <form onSubmit={handleSubmit}>
      <div className="mt-4 sm:mt-0 sm:ml-16 sm:flex-none">
        <div className="mt-6 flex items-center justify-end gap-x-6">
          <input
            className="file:mr-4 cursor-pointer file:cursor-pointer rounded file:border-0 bg-gray-100 file:bg-gray-500 file:px-4 file:py-2 text-sm text-black file:text-white file:hover:bg-gray-700"
            id="fileInput"
            type="file"
            accept="text/csv"
          ></input>
          <button
            type="submit"
            className="block rounded-md bg-lime-200 px-3 py-2 text-center text-sm font-semibold text-green-800 shadow-sm hover:bg-lime-300 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-lime-400"
          >
            <span className={"inline-flex items-center gap-x-1"}>
              <CloudArrowUpIcon
                className="h-6 w-6 shrink-0"
                aria-hidden="true"
              />
              Upload
            </span>
          </button>
        </div>
      </div>
    </form>
  );
}
