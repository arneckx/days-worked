'use client'

import {useCallback, useContext, useState} from 'react'
import {Listbox, ListboxButton, ListboxOption, ListboxOptions} from '@headlessui/react'
import {ChevronUpDownIcon} from '@heroicons/react/16/solid'
import {CheckIcon} from '@heroicons/react/20/solid'
import {BackofficeGatewayContext} from "@/src/context/BackofficeGatewayProvider";

const categories = [
    "RESTAURANT",
    "INSURANCE",
    "SALARY",
    "SOFTWARE",
    "HARDWARE",
    "CONFERENCE",
    "HOME",
    "CAR",
    "SOCIAL_SECURITY",
    "VAT",
    "TAXES",
    "BOOKKEEPER",
    "COMPANY",
    "NONE"
]

export default function TransactionCategory({transactionId, value}) {
    const [selected, setSelected] = useState(categories[categories.indexOf(value)])
    const backofficeGateway = useContext(BackofficeGatewayContext);

    const changeCategory = useCallback(async (category) => {
        try {
            await backofficeGateway
                .transactions()
                .changeCategory(transactionId, category)
        } catch (error) {
            console.log(error);
        }
    });

    return (
        <div className="w-32">
            <Listbox value={selected} onChange={(e) => {setSelected(e); changeCategory(e);}}>
                <div className="relative mt-2">
                    <ListboxButton
                        className="grid w-full cursor-default grid-cols-1 rounded-md bg-white pr-2 pl-3 text-left text-gray-900 outline outline-1 outline-gray-300 py-1.5 -outline-offset-1 focus:-outline-offset-2 focus:outline focus:outline-2 focus:outline-pink-600 sm:text-xs">
                        <span className="col-start-1 row-start-1 truncate pr-6">{selected}</span>
                        <ChevronUpDownIcon
                            aria-hidden="true"
                            className="col-start-1 row-start-1 self-center justify-self-end text-gray-500 size-5 sm:size-2"
                        />
                    </ListboxButton>

                    <ListboxOptions
                        transition
                        className="absolute z-10 mt-1 max-h-60 w-full overflow-auto rounded-md bg-white py-1 text-base shadow-lg ring-1 ring-black/5 focus:outline-none sm:text-xs data-[closed]:data-[leave]:opacity-0 data-[leave]:transition data-[leave]:duration-100 data-[leave]:ease-in"
                    >
                        {categories.map((category) => (
                            <ListboxOption
                                key={category}
                                value={category}
                                className="relative cursor-default select-none py-2 pr-9 pl-3 text-gray-900 group data-[focus]:bg-pink-600 data-[focus]:text-white data-[focus]:outline-none"
                            >
                                <span className="block truncate font-normal group-data-[selected]:font-semibold">{category}</span>

                                <span
                                    className="absolute inset-y-0 right-0 flex items-center pr-4 text-pink-600 group-[&:not([data-selected])]:hidden group-data-[focus]:text-white">
                                <CheckIcon aria-hidden="true" className="size-5"/>
                              </span>
                            </ListboxOption>
                        ))}
                    </ListboxOptions>
                </div>
            </Listbox>
        </div>
    )
}
