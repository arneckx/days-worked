"use client";

import ClippedText from "@/src/components/atoms/ClippedText";
import TransactionCategory from "@/src/components/transactions/TransactionCategory";

export default function TransactionRecord({ transaction }) {
  return (
    <tr key={transaction.transactionId} className={"even:bg-gray-50"}>
      <td className="whitespace-nowrap py-2 pr-3 pl-4 text-sm text-gray-900">
        {transaction.timestamp.toLocaleString("en-BE", {
          timeZone: "Europe/Brussels",
          weekday: "short",
          month: "short",
          day: "numeric",
        })}
      </td>
      <td className="whitespace-nowrap px-2 py-2 text-sm font-bold text-gray-900">
        {transaction.accountNumber}
      </td>
      <td className="px-2 py-2 text-sm text-gray-900">
        <ClippedText text={transaction.description} />
      </td>
      <td className="whitespace-nowrap px-2 py-2 text-sm text-gray-900">
        {transaction.type}
      </td>
      <td className="whitespace-nowrap px-2 py-2 text-sm font-bold text-gray-900">
        €{transaction.amount}
      </td>
      <td className="px-2 py-2 text-sm text-gray-900">
        <ClippedText text={transaction.noticeValue} />
      </td>
      <td className="whitespace-nowrap px-2 py-2 text-sm text-gray-900">
          <TransactionCategory value={transaction.category} transactionId={transaction.transactionId} />
      </td>
      <td className="relative whitespace-nowrap py-2 pr-4 pl-3 text-right text-sm font-medium">
        <a href="#" className="text-pink-600 hover:text-pink-900">
          Invoice
          <span className="sr-only">, {transaction.transactionId}</span>
        </a>
      </td>
    </tr>
  );
}
