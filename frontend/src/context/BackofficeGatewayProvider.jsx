"use client";

import {createContext} from "react";
import {BackofficeGateway} from "@/src/lib/backoffice-gateway";

export const BackofficeGatewayContext = createContext();

export default function BackofficeGatewayProvider(props) {
  const backofficeGateway = new BackofficeGateway(props.token);

  return (
    <BackofficeGatewayContext.Provider value={backofficeGateway}>
      {props.children}
    </BackofficeGatewayContext.Provider>
  );
}
