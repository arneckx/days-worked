import {CalendarGateway} from "@/src/lib/calendar/calendar-gateway";
import {TransactionsGateway} from "@/src/lib/transactions/transactions-gateway";
import {OfficeGateway} from "@/src/lib/office/office-gateway";

const backofficeapiurl = process.env.NEXT_PUBLIC_BACKOFFICE_API_URL;

export class BackofficeGateway {
  token;

  constructor(token) {
    this.token = token;
  }

  office() {
    return new OfficeGateway(this);
  }

  calendar() {
    return new CalendarGateway(this);
  }

  transactions() {
    return new TransactionsGateway(this);
  }

  async welcome() {
    return this.postBackoffice("WELCOME", {});
  }

  async postBackoffice(command, dto) {
    return await fetch(backofficeapiurl, {
      method: "post",
      headers: new Headers({
        Authorization: "Bearer " + this.token,
        Accept: "application/json",
        "Content-Type": "application/json",
        "X-Backoffice-Command": command,
      }),
      body: JSON.stringify(dto),
    })
      .then(this.#handleResponse)
      .catch(console.error);
  }

  async postFile(command, fileInput) {
    const formData = new FormData();
    let file = fileInput.files[0];
    formData.append("file", file, file.name);
    return await fetch(backofficeapiurl + "/files", {
      method: "post",
      headers: new Headers({
        Authorization: "Bearer " + this.token,
        Accept: "application/json",
        "X-Backoffice-Command": command,
      }),
      body: formData,
    })
      .then(this.#handleResponse)
      .catch(console.error);
  }

  #handleResponse(res) {
    const contentType = res.headers.get("content-type");
    if (contentType.includes("application/json")) {
      return res.json();
    }
    if (contentType.includes("text/plain")) {
      return res.text();
    }
    console.error("Response has no valid content type");
  }
}
