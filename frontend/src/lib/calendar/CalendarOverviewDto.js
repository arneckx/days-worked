export default class CalendarOverviewDto {
  totalNumberOfWorkedDays;
  months;

  constructor(totalNumberOfWorkedDays, months) {
    this.totalNumberOfWorkedDays = totalNumberOfWorkedDays;
    this.months = months;
  }

  static fromJson(data) {
    const months = new Map();
    data.months
      .map((month) => MonthlyOverviewDto.fromJson(month))
      .forEach((monthlyOverview) => {
        months.set(
          `${monthlyOverview.year}${monthlyOverview.month}`,
          monthlyOverview,
        );
      });
    return new CalendarOverviewDto(data.totalNumberOfWorkedDays, months);
  }

  findMonth(year, month) {
    let monthlyOverview = this.months.get(`${year}${month}`);
    return monthlyOverview ? monthlyOverview : MonthlyOverviewDto.empty();
  }
}

export class MonthlyOverviewDto {
  year;
  month;
  offdays;
  numberOfWorkedDays;
  stringOutput;

  constructor(year, month, offdays, numberOfWorkedDays, stringOutput) {
    this.year = year;
    this.month = month;
    this.offdays = offdays;
    this.numberOfWorkedDays = numberOfWorkedDays;
    this.stringOutput = stringOutput;
  }

  findOffday(date) {
    return this.offdays.get(
      `${date.getFullYear()}${date.getMonth() + 1}${date.getDate()}`,
    );
  }

  static fromJson(data) {
    const offdays = new Map();
    data.offdays
      .map((offday) => OffdayDto.from(offday))
      .forEach((offday) => {
        offdays.set(`${offday.year}${offday.month}${offday.day}`, offday);
      });

    return new MonthlyOverviewDto(
      data.year,
      data.month,
      offdays,
      data.numberOfWorkedDays,
      data.stringOutput,
    );
  }

  static empty() {
    return new MonthlyOverviewDto(
      undefined,
      undefined,
      new Map(),
      undefined,
      undefined,
    );
  }
}

export class OffdayDto {
  day;
  month;
  year;
  description;
  type;
  amount;

  constructor(day, month, year, description, type, amount) {
    this.day = day;
    this.month = month;
    this.year = year;
    this.description = description;
    this.type = type;
    this.amount = amount;
  }

  static from(data) {
    return new OffdayDto(
      data.day,
      data.month,
      data.year,
      data.description,
      data.type,
      data.amount,
    );
  }

  displayDescription() {
    return this.description;
  }

  displayDate() {
    const date = new Date(Date.parse(`${this.month}/${this.day}/${this.year}`));
    return date.toLocaleString("en-BE", {
      timeZone: "Europe/Brussels",
      weekday: "short",
      month: "short",
      day: "numeric",
    });
  }

  displayAmount() {
    switch (this.amount) {
      case "FULL":
        return "Full day";
      case "HALF":
        return "Half a day";
      case "QUARTER":
        return "Quarter of a day";
      default:
        return "Unknown";
    }
  }

  displayType() {
    return this.type === "OFFICIAL_HOLIDAY" ? "Official holiday" : "Vacation";
  }

  displayAmountAndType() {
    return `${this.displayType()}: ${this.displayAmount()}`;
  }

  static typeOf(displayType) {
    switch (displayType) {
      case "Official holiday":
        return "OFFICIAL_HOLIDAY";
      case "Vacation":
        return "VACATION";
      default:
        return "VACATION";
    }
  }

  static amountOf(displayAmount) {
    switch (displayAmount) {
      case "Full day":
        return "FULL";
      case "Half a day":
        return "HALF";
      case "Quarter of a day":
        return "QUARTER";
      default:
        return "FULL";
    }
  }
}
