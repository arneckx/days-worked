export default class CalendarStatsDto {
  totalWorkedDays;
  expectedWorkedDays;
  totalOffdays;

  constructor(totalWorkedDays, expectedWorkedDays, totalOffdays) {
    this.totalWorkedDays = totalWorkedDays;
    this.expectedWorkedDays = expectedWorkedDays;
    this.totalOffdays = totalOffdays;
  }

  static fromJson(data) {
    return new CalendarStatsDto(
      data.totalWorkedDays.toLocaleString("en-BE"),
      data.expectedWorkedDays.toLocaleString("en-BE"),
      data.totalOffdays.toLocaleString("en-BE"),
    );
  }
}
