export default class Day {
  date;
  dateAsObject;
  isCurrentMonth;
  isToday;
  offday;

  constructor(date, dateAsObject, isCurrentMonth, isToday, offday) {
    this.date = date;
    this.dateAsObject = dateAsObject;
    this.isCurrentMonth = isCurrentMonth;
    this.isToday = isToday;
    this.offday = offday;
  }

  static from(date, monthIndex, offday) {
    return new Day(
      `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`,
      date,
      Day.isThisMonth(date, monthIndex),
      Day.isToday(date),
      offday
        ? new Offday(offday.description, offday.type, offday.amount)
        : Offday.empty(),
    );
  }

  static fromDifferentMonth(date) {
    return new Day(
      `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`,
      false,
      false,
      null,
    );
  }

  static isToday(date) {
    const today = new Date();
    return (
      date.getFullYear() === today.getFullYear() &&
      date.getMonth() === today.getMonth() &&
      date.getDate() === today.getDate()
    );
  }

  static isThisMonth(date, monthIndex) {
    return date.getMonth() === monthIndex;
  }
}

export class Offday {
  description;
  type;
  amount;

  constructor(description, type, amount) {
    this.description = description;
    this.type = type;
    this.amount = amount;
  }

  static empty() {
    return new Offday(undefined, undefined, undefined);
  }

  displayAmount() {
    switch (this.amount) {
      case "FULL":
        return `Full day`;
      case "HALF":
        return `Half a day`;
      case "QUARTER":
        return `Quarter of a day`;
      default:
        return `Full day`;
    }
  }

  displayType() {
    return this.type === "OFFICIAL_HOLIDAY" ? "Official holiday" : "Vacation";
  }
}
