import Day from "@/src/lib/calendar/Day";
import { MonthlyOverviewDto } from "@/src/lib/calendar/CalendarOverviewDto";

export default class YearMonth {
  constructor(year, month) {
    this.year = year;
    this.month = month;
  }

  static ofToday() {
    const today = new Date();
    return new YearMonth(today.getFullYear(), today.getMonth() + 1);
  }

  nextMonth() {
    if (this.month === 12) {
      return new YearMonth(this.year + 1, 1);
    } else {
      return new YearMonth(this.year, this.month + 1);
    }
  }

  previousMonth() {
    if (this.month === 1) {
      return new YearMonth(this.year - 1, 12);
    } else {
      return new YearMonth(this.year, this.month - 1);
    }
  }

  allDays(offdaysOverview) {
    const monthlyOverview = offdaysOverview
      ? offdaysOverview.findMonth(this.year, this.month)
      : MonthlyOverviewDto.empty();

    const monthIndex = this.month - 1;
    const lastDayOfThisMonth = new Date(this.year, monthIndex + 1, 0);
    let nextDate = this.findBeginDayStartingAtAMonday(monthIndex);
    let days = [];

    while (nextDate <= lastDayOfThisMonth) {
      let date = new Date(nextDate);
      days.push(Day.from(date, monthIndex, monthlyOverview.findOffday(date)));
      nextDate.setDate(nextDate.getDate() + 1);
    }
    this.addRemainingDaysOfTheWeek(nextDate, days);

    return new Month(
      lastDayOfThisMonth.toLocaleString("en-BE", {
        timeZone: "Europe/Brussels",
        month: "long",
      }),
      monthlyOverview.numberOfWorkedDays,
      days,
      monthlyOverview.stringOutput,
    );
  }

  addRemainingDaysOfTheWeek(nextDate, days) {
    // Display the remaining days of the week
    nextDate.setDate(nextDate.getDate() - 1);
    const remainingDays = 6 - nextDate.getUTCDay();
    for (let i = 1; i <= remainingDays; i++) {
      const remainingDate = new Date(
        nextDate.getFullYear(),
        nextDate.getMonth(),
        nextDate.getDate() + i,
      );
      days.push(Day.fromDifferentMonth(remainingDate));
    }
  }

  findBeginDayStartingAtAMonday(monthIndex) {
    const firstDayOfThisMonth = new Date(this.year, monthIndex, 1);
    const dayOfTheWeek = firstDayOfThisMonth.getDay();
    const daysToSubtract = dayOfTheWeek === 0 ? 6 : dayOfTheWeek - 1;
    const newDate = new Date(firstDayOfThisMonth);
    newDate.setDate(firstDayOfThisMonth.getDate() - daysToSubtract);
    return newDate;
  }
}

class Month {
  name;
  numberOfWorkedDays;
  days;
  stringOutput;

  constructor(name, numberOfWorkedDays, days, stringOutput) {
    this.name = name;
    this.numberOfWorkedDays = numberOfWorkedDays;
    this.days = days;
    this.stringOutput = stringOutput;
  }
}
