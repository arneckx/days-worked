import CalendarOverviewDto, {
  OffdayDto,
} from "@/src/lib/calendar/CalendarOverviewDto";
import CalendarStatsDto from "@/src/lib/calendar/CalendarStatsDto";

export class CalendarGateway {
  backofficeGateway;

  constructor(backofficeGateway) {
    this.backofficeGateway = backofficeGateway;
  }

  async stats(year) {
    return this.backofficeGateway
      .postBackoffice("CALENDAR_YEAR_STATS", { year: year })
      .then((jsonBody) => CalendarStatsDto.fromJson(jsonBody.result));
  }

  async overviewFor(months) {
    return this.backofficeGateway
      .postBackoffice("CALENDAR_MONTHLY_OVERVIEW", months)
      .then((jsonBody) => CalendarOverviewDto.fromJson(jsonBody.result));
  }

  addOffday(day, month, year, description, type, amount) {
    this.backofficeGateway
      .postBackoffice("CALENDAR_ADD_DAYOFFS", [
        new OffdayDto(day, month, year, description, type, amount),
      ])
      .catch((error) => console.error(error));
  }

  deleteOffday(day, month, year) {
    this.backofficeGateway
      .postBackoffice("CALENDAR_DELETE_DAYOFF", new OffdayDto(day, month, year))
      .catch((error) => console.error(error));
  }
}
