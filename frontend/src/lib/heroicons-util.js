import {
    AcademicCapIcon,
    AdjustmentsHorizontalIcon,
    AdjustmentsVerticalIcon,
    ArchiveBoxArrowDownIcon,
    ArchiveBoxIcon,
    ArchiveBoxXMarkIcon,
    ArrowDownCircleIcon,
    ArrowDownIcon,
    ArrowDownLeftIcon,
    ArrowDownOnSquareIcon,
    ArrowDownOnSquareStackIcon,
    ArrowDownRightIcon,
    ArrowDownTrayIcon,
    ArrowLeftCircleIcon,
    ArrowLeftEndOnRectangleIcon,
    ArrowLeftIcon,
    ArrowLeftOnRectangleIcon,
    ArrowLeftStartOnRectangleIcon,
    ArrowLongDownIcon,
    ArrowLongLeftIcon,
    ArrowLongRightIcon,
    ArrowLongUpIcon,
    ArrowPathIcon,
    ArrowPathRoundedSquareIcon,
    ArrowRightCircleIcon,
    ArrowRightEndOnRectangleIcon,
    ArrowRightIcon,
    ArrowRightOnRectangleIcon,
    ArrowRightStartOnRectangleIcon,
    ArrowSmallDownIcon,
    ArrowSmallLeftIcon,
    ArrowSmallRightIcon,
    ArrowSmallUpIcon,
    ArrowsPointingInIcon,
    ArrowsPointingOutIcon,
    ArrowsRightLeftIcon,
    ArrowsUpDownIcon,
    ArrowTopRightOnSquareIcon,
    ArrowTrendingDownIcon,
    ArrowTrendingUpIcon,
    ArrowUpCircleIcon,
    ArrowUpIcon,
    ArrowUpLeftIcon,
    ArrowUpOnSquareIcon,
    ArrowUpOnSquareStackIcon,
    ArrowUpRightIcon,
    ArrowUpTrayIcon,
    ArrowUturnDownIcon,
    ArrowUturnLeftIcon,
    ArrowUturnRightIcon,
    ArrowUturnUpIcon,
    AtSymbolIcon,
    BackspaceIcon,
    BackwardIcon,
    BanknotesIcon,
    Bars2Icon,
    Bars3BottomLeftIcon,
    Bars3BottomRightIcon,
    Bars3CenterLeftIcon,
    Bars3Icon,
    Bars4Icon,
    BarsArrowDownIcon,
    BarsArrowUpIcon,
    Battery0Icon,
    Battery100Icon,
    Battery50Icon,
    BeakerIcon,
    BellAlertIcon,
    BellIcon,
    BellSlashIcon,
    BellSnoozeIcon,
    BoltIcon,
    BoltSlashIcon,
    BookmarkIcon,
    BookmarkSlashIcon,
    BookmarkSquareIcon,
    BookOpenIcon,
    BriefcaseIcon,
    BugAntIcon,
    BuildingLibraryIcon,
    BuildingOffice2Icon,
    BuildingOfficeIcon,
    BuildingStorefrontIcon,
    CakeIcon,
    CalculatorIcon,
    CalendarDaysIcon,
    CalendarIcon,
    CameraIcon,
    ChartBarIcon,
    ChartBarSquareIcon,
    ChartPieIcon,
    ChatBubbleBottomCenterIcon,
    ChatBubbleBottomCenterTextIcon,
    ChatBubbleLeftEllipsisIcon,
    ChatBubbleLeftIcon,
    ChatBubbleLeftRightIcon,
    ChatBubbleOvalLeftEllipsisIcon,
    ChatBubbleOvalLeftIcon,
    CheckBadgeIcon,
    CheckCircleIcon,
    CheckIcon,
    ChevronDoubleDownIcon,
    ChevronDoubleLeftIcon,
    ChevronDoubleRightIcon,
    ChevronDoubleUpIcon,
    ChevronDownIcon,
    ChevronLeftIcon,
    ChevronRightIcon,
    ChevronUpDownIcon,
    ChevronUpIcon,
    CircleStackIcon,
    ClipboardDocumentCheckIcon,
    ClipboardDocumentIcon,
    ClipboardDocumentListIcon,
    ClipboardIcon,
    ClockIcon,
    CloudArrowDownIcon,
    CloudArrowUpIcon,
    CloudIcon,
    CodeBracketIcon,
    CodeBracketSquareIcon,
    Cog6ToothIcon,
    Cog8ToothIcon,
    CogIcon,
    CommandLineIcon,
    ComputerDesktopIcon,
    CpuChipIcon,
    CreditCardIcon,
    CubeIcon,
    CubeTransparentIcon,
    CurrencyBangladeshiIcon,
    CurrencyDollarIcon,
    CurrencyEuroIcon,
    CurrencyPoundIcon,
    CurrencyRupeeIcon,
    CurrencyYenIcon,
    CursorArrowRaysIcon,
    CursorArrowRippleIcon,
    DevicePhoneMobileIcon,
    DeviceTabletIcon,
    DocumentArrowDownIcon,
    DocumentArrowUpIcon,
    DocumentChartBarIcon,
    DocumentCheckIcon,
    DocumentDuplicateIcon,
    DocumentIcon,
    DocumentMagnifyingGlassIcon,
    DocumentMinusIcon,
    DocumentPlusIcon,
    DocumentTextIcon,
    EllipsisHorizontalCircleIcon,
    EllipsisHorizontalIcon,
    EllipsisVerticalIcon,
    EnvelopeIcon,
    EnvelopeOpenIcon,
    ExclamationCircleIcon,
    ExclamationTriangleIcon,
    EyeDropperIcon,
    EyeIcon,
    EyeSlashIcon,
    FaceFrownIcon,
    FaceSmileIcon,
    FilmIcon,
    FingerPrintIcon,
    FireIcon,
    FlagIcon,
    FolderArrowDownIcon,
    FolderIcon,
    FolderMinusIcon,
    FolderOpenIcon,
    FolderPlusIcon,
    ForwardIcon,
    FunnelIcon,
    GifIcon,
    GiftIcon,
    GiftTopIcon,
    GlobeAltIcon,
    GlobeAmericasIcon,
    GlobeAsiaAustraliaIcon,
    GlobeEuropeAfricaIcon,
    HandRaisedIcon,
    HandThumbDownIcon,
    HandThumbUpIcon,
    HashtagIcon,
    HeartIcon,
    HomeIcon,
    HomeModernIcon,
    IdentificationIcon,
    InboxArrowDownIcon,
    InboxIcon,
    InboxStackIcon,
    InformationCircleIcon,
    KeyIcon,
    LanguageIcon,
    LifebuoyIcon,
    LightBulbIcon,
    LinkIcon,
    ListBulletIcon,
    LockClosedIcon,
    LockOpenIcon,
    MagnifyingGlassCircleIcon,
    MagnifyingGlassIcon,
    MagnifyingGlassMinusIcon,
    MagnifyingGlassPlusIcon,
    MapIcon,
    MapPinIcon,
    MegaphoneIcon,
    MicrophoneIcon,
    MinusCircleIcon,
    MinusIcon,
    MinusSmallIcon,
    MoonIcon,
    MusicalNoteIcon,
    NewspaperIcon,
    NoSymbolIcon,
    PaintBrushIcon,
    PaperAirplaneIcon,
    PaperClipIcon,
    PauseCircleIcon,
    PauseIcon,
    PencilIcon,
    PencilSquareIcon,
    PhoneArrowDownLeftIcon,
    PhoneArrowUpRightIcon,
    PhoneIcon,
    PhoneXMarkIcon,
    PhotoIcon,
    PlayCircleIcon,
    PlayIcon,
    PlayPauseIcon,
    PlusCircleIcon,
    PlusIcon,
    PlusSmallIcon,
    PowerIcon,
    PresentationChartBarIcon,
    PresentationChartLineIcon,
    PrinterIcon,
    PuzzlePieceIcon,
    QrCodeIcon,
    QuestionMarkCircleIcon,
    QueueListIcon,
    RadioIcon,
    ReceiptPercentIcon,
    ReceiptRefundIcon,
    RectangleGroupIcon,
    RectangleStackIcon,
    RocketLaunchIcon,
    RssIcon,
    ScaleIcon,
    ScissorsIcon,
    ServerIcon,
    ServerStackIcon,
    ShareIcon,
    ShieldCheckIcon,
    ShieldExclamationIcon,
    ShoppingBagIcon,
    ShoppingCartIcon,
    SignalIcon,
    SignalSlashIcon,
    SparklesIcon,
    SpeakerWaveIcon,
    SpeakerXMarkIcon,
    Square2StackIcon,
    Square3Stack3DIcon,
    Squares2X2Icon,
    SquaresPlusIcon,
    StarIcon,
    StopCircleIcon,
    StopIcon,
    SunIcon,
    SwatchIcon,
    TableCellsIcon,
    TagIcon,
    TicketIcon,
    TrashIcon,
    TrophyIcon,
    TruckIcon,
    TvIcon,
    UserCircleIcon,
    UserGroupIcon,
    UserIcon,
    UserMinusIcon,
    UserPlusIcon,
    UsersIcon,
    VariableIcon,
    VideoCameraIcon,
    VideoCameraSlashIcon,
    ViewColumnsIcon,
    ViewfinderCircleIcon,
    WalletIcon,
    WifiIcon,
    WindowIcon,
    WrenchIcon,
    WrenchScrewdriverIcon,
    XCircleIcon,
    XMarkIcon,
} from "@heroicons/react/20/solid";

export function findIcon(iconName) {
    switch (iconName) {
        case 'AcademicCapIcon':
            return <AcademicCapIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'AdjustmentsHorizontalIcon':
            return <AdjustmentsHorizontalIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'AdjustmentsVerticalIcon':
            return <AdjustmentsVerticalIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArchiveBoxArrowDownIcon':
            return <ArchiveBoxArrowDownIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArchiveBoxXMarkIcon':
            return <ArchiveBoxXMarkIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArchiveBoxIcon':
            return <ArchiveBoxIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowDownCircleIcon':
            return <ArrowDownCircleIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowDownLeftIcon':
            return <ArrowDownLeftIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowDownOnSquareStackIcon':
            return <ArrowDownOnSquareStackIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowDownOnSquareIcon':
            return <ArrowDownOnSquareIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowDownRightIcon':
            return <ArrowDownRightIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowDownTrayIcon':
            return <ArrowDownTrayIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowDownIcon':
            return <ArrowDownIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowLeftCircleIcon':
            return <ArrowLeftCircleIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowLeftEndOnRectangleIcon':
            return <ArrowLeftEndOnRectangleIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowLeftOnRectangleIcon':
            return <ArrowLeftOnRectangleIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowLeftStartOnRectangleIcon':
            return <ArrowLeftStartOnRectangleIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowLeftIcon':
            return <ArrowLeftIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowLongDownIcon':
            return <ArrowLongDownIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowLongLeftIcon':
            return <ArrowLongLeftIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowLongRightIcon':
            return <ArrowLongRightIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowLongUpIcon':
            return <ArrowLongUpIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowPathRoundedSquareIcon':
            return <ArrowPathRoundedSquareIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowPathIcon':
            return <ArrowPathIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowRightCircleIcon':
            return <ArrowRightCircleIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowRightEndOnRectangleIcon':
            return <ArrowRightEndOnRectangleIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowRightOnRectangleIcon':
            return <ArrowRightOnRectangleIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowRightStartOnRectangleIcon':
            return <ArrowRightStartOnRectangleIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowRightIcon':
            return <ArrowRightIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowSmallDownIcon':
            return <ArrowSmallDownIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowSmallLeftIcon':
            return <ArrowSmallLeftIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowSmallRightIcon':
            return <ArrowSmallRightIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowSmallUpIcon':
            return <ArrowSmallUpIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowTopRightOnSquareIcon':
            return <ArrowTopRightOnSquareIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowTrendingDownIcon':
            return <ArrowTrendingDownIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowTrendingUpIcon':
            return <ArrowTrendingUpIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowUpCircleIcon':
            return <ArrowUpCircleIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowUpLeftIcon':
            return <ArrowUpLeftIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowUpOnSquareStackIcon':
            return <ArrowUpOnSquareStackIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowUpOnSquareIcon':
            return <ArrowUpOnSquareIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowUpRightIcon':
            return <ArrowUpRightIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowUpTrayIcon':
            return <ArrowUpTrayIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowUpIcon':
            return <ArrowUpIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowUturnDownIcon':
            return <ArrowUturnDownIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowUturnLeftIcon':
            return <ArrowUturnLeftIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowUturnRightIcon':
            return <ArrowUturnRightIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowUturnUpIcon':
            return <ArrowUturnUpIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowsPointingInIcon':
            return <ArrowsPointingInIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowsPointingOutIcon':
            return <ArrowsPointingOutIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowsRightLeftIcon':
            return <ArrowsRightLeftIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ArrowsUpDownIcon':
            return <ArrowsUpDownIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'AtSymbolIcon':
            return <AtSymbolIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'BackspaceIcon':
            return <BackspaceIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'BackwardIcon':
            return <BackwardIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'BanknotesIcon':
            return <BanknotesIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'Bars2Icon':
            return <Bars2Icon aria-hidden="true" className="h-5 w-5"/>;
        case 'Bars3BottomLeftIcon':
            return <Bars3BottomLeftIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'Bars3BottomRightIcon':
            return <Bars3BottomRightIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'Bars3CenterLeftIcon':
            return <Bars3CenterLeftIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'Bars3Icon':
            return <Bars3Icon aria-hidden="true" className="h-5 w-5"/>;
        case 'Bars4Icon':
            return <Bars4Icon aria-hidden="true" className="h-5 w-5"/>;
        case 'BarsArrowDownIcon':
            return <BarsArrowDownIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'BarsArrowUpIcon':
            return <BarsArrowUpIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'Battery0Icon':
            return <Battery0Icon aria-hidden="true" className="h-5 w-5"/>;
        case 'Battery100Icon':
            return <Battery100Icon aria-hidden="true" className="h-5 w-5"/>;
        case 'Battery50Icon':
            return <Battery50Icon aria-hidden="true" className="h-5 w-5"/>;
        case 'BeakerIcon':
            return <BeakerIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'BellAlertIcon':
            return <BellAlertIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'BellSlashIcon':
            return <BellSlashIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'BellSnoozeIcon':
            return <BellSnoozeIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'BellIcon':
            return <BellIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'BoltSlashIcon':
            return <BoltSlashIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'BoltIcon':
            return <BoltIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'BookOpenIcon':
            return <BookOpenIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'BookmarkSlashIcon':
            return <BookmarkSlashIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'BookmarkSquareIcon':
            return <BookmarkSquareIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'BookmarkIcon':
            return <BookmarkIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'BriefcaseIcon':
            return <BriefcaseIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'BugAntIcon':
            return <BugAntIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'BuildingLibraryIcon':
            return <BuildingLibraryIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'BuildingOffice2Icon':
            return <BuildingOffice2Icon aria-hidden="true" className="h-5 w-5"/>;
        case 'BuildingOfficeIcon':
            return <BuildingOfficeIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'BuildingStorefrontIcon':
            return <BuildingStorefrontIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'CakeIcon':
            return <CakeIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'CalculatorIcon':
            return <CalculatorIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'CalendarDaysIcon':
            return <CalendarDaysIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'CalendarIcon':
            return <CalendarIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'CameraIcon':
            return <CameraIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ChartBarSquareIcon':
            return <ChartBarSquareIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ChartBarIcon':
            return <ChartBarIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ChartPieIcon':
            return <ChartPieIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ChatBubbleBottomCenterTextIcon':
            return <ChatBubbleBottomCenterTextIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ChatBubbleBottomCenterIcon':
            return <ChatBubbleBottomCenterIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ChatBubbleLeftEllipsisIcon':
            return <ChatBubbleLeftEllipsisIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ChatBubbleLeftRightIcon':
            return <ChatBubbleLeftRightIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ChatBubbleLeftIcon':
            return <ChatBubbleLeftIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ChatBubbleOvalLeftEllipsisIcon':
            return <ChatBubbleOvalLeftEllipsisIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ChatBubbleOvalLeftIcon':
            return <ChatBubbleOvalLeftIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'CheckBadgeIcon':
            return <CheckBadgeIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'CheckCircleIcon':
            return <CheckCircleIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'CheckIcon':
            return <CheckIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ChevronDoubleDownIcon':
            return <ChevronDoubleDownIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ChevronDoubleLeftIcon':
            return <ChevronDoubleLeftIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ChevronDoubleRightIcon':
            return <ChevronDoubleRightIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ChevronDoubleUpIcon':
            return <ChevronDoubleUpIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ChevronDownIcon':
            return <ChevronDownIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ChevronLeftIcon':
            return <ChevronLeftIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ChevronRightIcon':
            return <ChevronRightIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ChevronUpDownIcon':
            return <ChevronUpDownIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ChevronUpIcon':
            return <ChevronUpIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'CircleStackIcon':
            return <CircleStackIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ClipboardDocumentCheckIcon':
            return <ClipboardDocumentCheckIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ClipboardDocumentListIcon':
            return <ClipboardDocumentListIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ClipboardDocumentIcon':
            return <ClipboardDocumentIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ClipboardIcon':
            return <ClipboardIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ClockIcon':
            return <ClockIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'CloudArrowDownIcon':
            return <CloudArrowDownIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'CloudArrowUpIcon':
            return <CloudArrowUpIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'CloudIcon':
            return <CloudIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'CodeBracketSquareIcon':
            return <CodeBracketSquareIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'CodeBracketIcon':
            return <CodeBracketIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'Cog6ToothIcon':
            return <Cog6ToothIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'Cog8ToothIcon':
            return <Cog8ToothIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'CogIcon':
            return <CogIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'CommandLineIcon':
            return <CommandLineIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ComputerDesktopIcon':
            return <ComputerDesktopIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'CpuChipIcon':
            return <CpuChipIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'CreditCardIcon':
            return <CreditCardIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'CubeTransparentIcon':
            return <CubeTransparentIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'CubeIcon':
            return <CubeIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'CurrencyBangladeshiIcon':
            return <CurrencyBangladeshiIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'CurrencyDollarIcon':
            return <CurrencyDollarIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'CurrencyEuroIcon':
            return <CurrencyEuroIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'CurrencyPoundIcon':
            return <CurrencyPoundIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'CurrencyRupeeIcon':
            return <CurrencyRupeeIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'CurrencyYenIcon':
            return <CurrencyYenIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'CursorArrowRaysIcon':
            return <CursorArrowRaysIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'CursorArrowRippleIcon':
            return <CursorArrowRippleIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'DevicePhoneMobileIcon':
            return <DevicePhoneMobileIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'DeviceTabletIcon':
            return <DeviceTabletIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'DocumentArrowDownIcon':
            return <DocumentArrowDownIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'DocumentArrowUpIcon':
            return <DocumentArrowUpIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'DocumentChartBarIcon':
            return <DocumentChartBarIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'DocumentCheckIcon':
            return <DocumentCheckIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'DocumentDuplicateIcon':
            return <DocumentDuplicateIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'DocumentMagnifyingGlassIcon':
            return <DocumentMagnifyingGlassIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'DocumentMinusIcon':
            return <DocumentMinusIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'DocumentPlusIcon':
            return <DocumentPlusIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'DocumentTextIcon':
            return <DocumentTextIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'DocumentIcon':
            return <DocumentIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'EllipsisHorizontalCircleIcon':
            return <EllipsisHorizontalCircleIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'EllipsisHorizontalIcon':
            return <EllipsisHorizontalIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'EllipsisVerticalIcon':
            return <EllipsisVerticalIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'EnvelopeOpenIcon':
            return <EnvelopeOpenIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'EnvelopeIcon':
            return <EnvelopeIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ExclamationCircleIcon':
            return <ExclamationCircleIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ExclamationTriangleIcon':
            return <ExclamationTriangleIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'EyeDropperIcon':
            return <EyeDropperIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'EyeSlashIcon':
            return <EyeSlashIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'EyeIcon':
            return <EyeIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'FaceFrownIcon':
            return <FaceFrownIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'FaceSmileIcon':
            return <FaceSmileIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'FilmIcon':
            return <FilmIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'FingerPrintIcon':
            return <FingerPrintIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'FireIcon':
            return <FireIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'FlagIcon':
            return <FlagIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'FolderArrowDownIcon':
            return <FolderArrowDownIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'FolderMinusIcon':
            return <FolderMinusIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'FolderOpenIcon':
            return <FolderOpenIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'FolderPlusIcon':
            return <FolderPlusIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'FolderIcon':
            return <FolderIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ForwardIcon':
            return <ForwardIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'FunnelIcon':
            return <FunnelIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'GifIcon':
            return <GifIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'GiftTopIcon':
            return <GiftTopIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'GiftIcon':
            return <GiftIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'GlobeAltIcon':
            return <GlobeAltIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'GlobeAmericasIcon':
            return <GlobeAmericasIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'GlobeAsiaAustraliaIcon':
            return <GlobeAsiaAustraliaIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'GlobeEuropeAfricaIcon':
            return <GlobeEuropeAfricaIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'HandRaisedIcon':
            return <HandRaisedIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'HandThumbDownIcon':
            return <HandThumbDownIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'HandThumbUpIcon':
            return <HandThumbUpIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'HashtagIcon':
            return <HashtagIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'HeartIcon':
            return <HeartIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'HomeModernIcon':
            return <HomeModernIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'HomeIcon':
            return <HomeIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'IdentificationIcon':
            return <IdentificationIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'InboxArrowDownIcon':
            return <InboxArrowDownIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'InboxStackIcon':
            return <InboxStackIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'InboxIcon':
            return <InboxIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'InformationCircleIcon':
            return <InformationCircleIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'KeyIcon':
            return <KeyIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'LanguageIcon':
            return <LanguageIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'LifebuoyIcon':
            return <LifebuoyIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'LightBulbIcon':
            return <LightBulbIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'LinkIcon':
            return <LinkIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ListBulletIcon':
            return <ListBulletIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'LockClosedIcon':
            return <LockClosedIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'LockOpenIcon':
            return <LockOpenIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'MagnifyingGlassCircleIcon':
            return <MagnifyingGlassCircleIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'MagnifyingGlassMinusIcon':
            return <MagnifyingGlassMinusIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'MagnifyingGlassPlusIcon':
            return <MagnifyingGlassPlusIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'MagnifyingGlassIcon':
            return <MagnifyingGlassIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'MapPinIcon':
            return <MapPinIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'MapIcon':
            return <MapIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'MegaphoneIcon':
            return <MegaphoneIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'MicrophoneIcon':
            return <MicrophoneIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'MinusCircleIcon':
            return <MinusCircleIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'MinusSmallIcon':
            return <MinusSmallIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'MinusIcon':
            return <MinusIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'MoonIcon':
            return <MoonIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'MusicalNoteIcon':
            return <MusicalNoteIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'NewspaperIcon':
            return <NewspaperIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'NoSymbolIcon':
            return <NoSymbolIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'PaintBrushIcon':
            return <PaintBrushIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'PaperAirplaneIcon':
            return <PaperAirplaneIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'PaperClipIcon':
            return <PaperClipIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'PauseCircleIcon':
            return <PauseCircleIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'PauseIcon':
            return <PauseIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'PencilSquareIcon':
            return <PencilSquareIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'PencilIcon':
            return <PencilIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'PhoneArrowDownLeftIcon':
            return <PhoneArrowDownLeftIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'PhoneArrowUpRightIcon':
            return <PhoneArrowUpRightIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'PhoneXMarkIcon':
            return <PhoneXMarkIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'PhoneIcon':
            return <PhoneIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'PhotoIcon':
            return <PhotoIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'PlayCircleIcon':
            return <PlayCircleIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'PlayPauseIcon':
            return <PlayPauseIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'PlayIcon':
            return <PlayIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'PlusCircleIcon':
            return <PlusCircleIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'PlusSmallIcon':
            return <PlusSmallIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'PlusIcon':
            return <PlusIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'PowerIcon':
            return <PowerIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'PresentationChartBarIcon':
            return <PresentationChartBarIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'PresentationChartLineIcon':
            return <PresentationChartLineIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'PrinterIcon':
            return <PrinterIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'PuzzlePieceIcon':
            return <PuzzlePieceIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'QrCodeIcon':
            return <QrCodeIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'QuestionMarkCircleIcon':
            return <QuestionMarkCircleIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'QueueListIcon':
            return <QueueListIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'RadioIcon':
            return <RadioIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ReceiptPercentIcon':
            return <ReceiptPercentIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ReceiptRefundIcon':
            return <ReceiptRefundIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'RectangleGroupIcon':
            return <RectangleGroupIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'RectangleStackIcon':
            return <RectangleStackIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'RocketLaunchIcon':
            return <RocketLaunchIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'RssIcon':
            return <RssIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ScaleIcon':
            return <ScaleIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ScissorsIcon':
            return <ScissorsIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ServerStackIcon':
            return <ServerStackIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ServerIcon':
            return <ServerIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ShareIcon':
            return <ShareIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ShieldCheckIcon':
            return <ShieldCheckIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ShieldExclamationIcon':
            return <ShieldExclamationIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ShoppingBagIcon':
            return <ShoppingBagIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ShoppingCartIcon':
            return <ShoppingCartIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'SignalSlashIcon':
            return <SignalSlashIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'SignalIcon':
            return <SignalIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'SparklesIcon':
            return <SparklesIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'SpeakerWaveIcon':
            return <SpeakerWaveIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'SpeakerXMarkIcon':
            return <SpeakerXMarkIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'Square2StackIcon':
            return <Square2StackIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'Square3Stack3DIcon':
            return <Square3Stack3DIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'Squares2X2Icon':
            return <Squares2X2Icon aria-hidden="true" className="h-5 w-5"/>;
        case 'SquaresPlusIcon':
            return <SquaresPlusIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'StarIcon':
            return <StarIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'StopCircleIcon':
            return <StopCircleIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'StopIcon':
            return <StopIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'SunIcon':
            return <SunIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'SwatchIcon':
            return <SwatchIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'TableCellsIcon':
            return <TableCellsIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'TagIcon':
            return <TagIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'TicketIcon':
            return <TicketIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'TrashIcon':
            return <TrashIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'TrophyIcon':
            return <TrophyIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'TruckIcon':
            return <TruckIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'TvIcon':
            return <TvIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'UserCircleIcon':
            return <UserCircleIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'UserGroupIcon':
            return <UserGroupIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'UserMinusIcon':
            return <UserMinusIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'UserPlusIcon':
            return <UserPlusIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'UserIcon':
            return <UserIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'UsersIcon':
            return <UsersIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'VariableIcon':
            return <VariableIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'VideoCameraSlashIcon':
            return <VideoCameraSlashIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'VideoCameraIcon':
            return <VideoCameraIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ViewColumnsIcon':
            return <ViewColumnsIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'ViewfinderCircleIcon':
            return <ViewfinderCircleIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'WalletIcon':
            return <WalletIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'WifiIcon':
            return <WifiIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'WindowIcon':
            return <WindowIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'WrenchScrewdriverIcon':
            return <WrenchScrewdriverIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'WrenchIcon':
            return <WrenchIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'XCircleIcon':
            return <XCircleIcon aria-hidden="true" className="h-5 w-5"/>;
        case 'XMarkIcon':
            return <XMarkIcon aria-hidden="true" className="h-5 w-5"/>;
        default:
            return <QuestionMarkCircleIcon aria-hidden="true" className="h-5 w-5"/>;
    }
}
