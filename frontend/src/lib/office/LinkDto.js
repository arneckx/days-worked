export default class LinkDto {
  order;
  name;
  href;
  initial;


  constructor(order, name, href, initial) {
    this.order = order;
    this.name = name;
    this.href = href;
    this.initial = initial;
  }

  static fromJson(data) {
    return new LinkDto(
      data.order,
      data.name,
      data.href,
      data.initial,
    );
  }
}
