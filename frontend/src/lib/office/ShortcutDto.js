export default class ShortcutDto {
    order;
    name;
    icon;
    href;
    bgColor;


    constructor(order, name, icon, href, bgColor) {
        this.order = order;
        this.name = name;
        this.icon = icon;
        this.href = href;
        this.bgColor = bgColor;
    }

    static fromJson(data) {
        return new ShortcutDto(
            data.order,
            data.name,
            data.icon,
            data.href,
            data.bgColor,
        );
    }
}