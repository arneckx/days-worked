import LinkDto from "@/src/lib/office/LinkDto";
import ShortcutDto from "@/src/lib/office/ShortcutDto";

export class OfficeGateway {
    backofficeGateway;

    constructor(backofficeGateway) {
        this.backofficeGateway = backofficeGateway;
    }

    async links() {
        return this.backofficeGateway
            .postBackoffice("OFFICE_LINKS", {})
            .then(jsonBody => jsonBody.result.links.map(LinkDto.fromJson).sort((a, b) => a.order - b.order));
    }

    async shortcuts() {
        return this.backofficeGateway
            .postBackoffice("OFFICE_SHORTCUTS", {})
            .then(jsonBody => jsonBody.result.links.map(ShortcutDto.fromJson).sort((a, b) => a.order - b.order));
    }
}
