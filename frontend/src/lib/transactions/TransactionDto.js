export default class TransactionDto {
  transactionId;
  timestamp;
  accountNumber;
  description;
  type;
  amount;
  balance;
  noticeValue;
  noticeIsStructured;
  category;

  constructor(
    transactionId,
    timestamp,
    accountNumber,
    description,
    type,
    amount,
    balance,
    noticeValue,
    noticeIsStructured,
    category,
  ) {
    this.transactionId = transactionId;
    this.timestamp = timestamp;
    this.accountNumber = accountNumber;
    this.description = description;
    this.type = type;
    this.amount = amount;
    this.balance = balance;
    this.noticeValue = noticeValue;
    this.noticeIsStructured = noticeIsStructured;
    this.category = category;
  }

  static fromJson(data) {
    const timestamp = new Date(0);
    timestamp.setUTCSeconds(data.timestamp);
    return new TransactionDto(
      data.transactionId,
      timestamp,
      data.accountNumber,
      data.description,
      data.type,
      data.amount.toLocaleString("en-BE"),
      data.balance.toLocaleString("en-BE"),
      data.noticeValue,
      data.noticeIsStructured,
      data.category,
    );
  }
}
