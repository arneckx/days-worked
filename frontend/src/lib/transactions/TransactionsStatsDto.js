export default class TransactionStatsDto {
  amountOfTransactions;
  incomingTransaction;
  outgoingTransaction;
  categories;

  constructor(amountOfTransactions, incomingTransaction, outgoingTransaction, categories) {
    this.amountOfTransactions = amountOfTransactions;
    this.incomingTransaction = incomingTransaction;
    this.outgoingTransaction = outgoingTransaction;
    this.categories = categories;
  }

  static fromJson(data) {
    return new TransactionStatsDto(
      data.amountOfTransactions.toLocaleString("en-BE"),
      "€ " + data.incomingTransaction.toLocaleString("en-BE"),
        "€ " + data.outgoingTransaction.toLocaleString("en-BE"),
        data.categories
    );
  }
}
