import TransactionDto from "@/src/lib/transactions/TransactionDto";
import TransactionStatsDto from "@/src/lib/transactions/TransactionsStatsDto";

export class TransactionsGateway {
    backofficeGateway;

    constructor(backofficeGateway) {
        this.backofficeGateway = backofficeGateway;
    }

    async stats(year) {
        return this.backofficeGateway
            .postBackoffice("TRANSACTIONS_YEAR_STATS", {year: year})
            .then((jsonBody) => TransactionStatsDto.fromJson(jsonBody.result));
    }

    importTransactions(fileInput) {
        this.backofficeGateway
            .postFile("TRANSACTIONS_IMPORT", fileInput)
            .catch((error) => console.error(error));
    }

    findAll(page, size) {
        return this.backofficeGateway
            .postBackoffice("TRANSACTIONS_LIST", {page: page, size: size})
            .then((jsonBody) =>
                jsonBody.result.transactions.map(TransactionDto.fromJson),
            );
    }

    changeCategory(transactionId, category) {
        return this.backofficeGateway
            .postBackoffice("TRANSACTIONS_CHANGE_CATEGORY", {transactionId: transactionId, transactionCategory: category});
    }
}
