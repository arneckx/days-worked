import {withAuth} from "next-auth/middleware";

export default withAuth({
  callbacks: {
    authorized({ req, token }) {
      return token && token.exp > Date.now() / 1000;
    },
  },
});

export const config = { matcher: ["/:path*"] };
