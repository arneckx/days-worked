resource "aws_cognito_user_pool" "backoffice_pool" {
  name = "backoffice"

  admin_create_user_config {
    allow_admin_create_user_only = true
  }

  account_recovery_setting {
    recovery_mechanism {
      name     = "verified_email"
      priority = 1
    }
  }
}

resource "aws_cognito_user" "arnehendrickx" {
  user_pool_id = aws_cognito_user_pool.backoffice_pool.id
  username     = "arnehendrickx"

  enabled = true

  attributes = {
    email                 = "info@arneckx.be"
    email_verified        = true
    phone_number          = "+32494864805"
    phone_number_verified = true
  }

  desired_delivery_mediums = [
    "EMAIL"
  ]
}

resource "aws_cognito_user_pool_domain" "main" {
  domain       = "backoffice-arneckx"
  user_pool_id = aws_cognito_user_pool.backoffice_pool.id
}

resource "aws_cognito_user_pool_client" "backoffice_client" {
  name         = "backoffice"
  user_pool_id = aws_cognito_user_pool.backoffice_pool.id

  access_token_validity = 60
  allowed_oauth_flows = [
    "code",
  ]
  allowed_oauth_flows_user_pool_client = true
  allowed_oauth_scopes = [
    "email",
    "openid",
    "phone",
  ]
  auth_session_validity = 15
  callback_urls = [
    "https://backoffice.arneckx.be/api/auth/callback/cognito",
    "http://localhost:3000/api/auth/callback/cognito",
  ]

  enable_propagate_additional_user_context_data = false
  enable_token_revocation                       = true
  explicit_auth_flows = [
    "ALLOW_REFRESH_TOKEN_AUTH",
    "ALLOW_USER_SRP_AUTH",
  ]
  generate_secret   = true
  id_token_validity = 60
  logout_urls       = []

  prevent_user_existence_errors = "ENABLED"
  read_attributes = [
    "email",
    "email_verified",
    "family_name",
    "given_name",
    "name",
    "nickname",
    "phone_number",
    "phone_number_verified",
  ]
  refresh_token_validity = 1
  supported_identity_providers = [
    "COGNITO",
  ]
  write_attributes = [
    "email",
    "phone_number",
  ]

  token_validity_units {
    access_token  = "minutes"
    id_token      = "minutes"
    refresh_token = "days"
  }


}