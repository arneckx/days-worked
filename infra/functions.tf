resource "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_lambda_function" "backoffice" {
  filename      = var.build_path
  function_name = "backoffice"
  role          = aws_iam_role.iam_for_lambda.arn
  handler       = "io.quarkus.amazon.lambda.runtime.QuarkusStreamHandler::handleRequest"
  timeout       = 60

  source_code_hash = filebase64sha256(var.build_path)

  runtime = "java17"

  memory_size = 512
  publish     = true
  snap_start {
    apply_on = "PublishedVersions"
  }

  lifecycle {
    ignore_changes = [
      environment,
    ]
  }
}

resource "aws_lambda_alias" "backoffice_lambda_alias" {
  name             = "latest_version"
  function_name    = aws_lambda_function.backoffice.arn
  function_version = aws_lambda_function.backoffice.version
}

resource "aws_lambda_function" "files" {
  filename      = var.build_path
  function_name = "files"
  role          = aws_iam_role.iam_for_lambda.arn
  handler       = "io.quarkus.amazon.lambda.runtime.QuarkusStreamHandler::handleRequest"
  timeout       = 60

  source_code_hash = filebase64sha256(var.build_path)

  runtime = "java17"

  memory_size = 512
  publish     = true
  snap_start {
    apply_on = "PublishedVersions"
  }

  lifecycle {
    ignore_changes = [
      environment,
    ]
  }
}

resource "aws_lambda_alias" "files_lambda_alias" {
  name             = "latest_version"
  function_name    = aws_lambda_function.files.arn
  function_version = aws_lambda_function.files.version
}


data "aws_iam_policy_document" "lambda_logging" {
  statement {
    effect = "Allow"

    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]

    resources = ["arn:aws:logs:*:*:*"]
  }
}

resource "aws_iam_policy" "lambda_logging" {
  name        = "lambda_logging"
  path        = "/"
  description = "IAM policy for logging from a lambda"
  policy      = data.aws_iam_policy_document.lambda_logging.json
}

resource "aws_iam_role_policy_attachment" "lambda_logs" {
  role       = aws_iam_role.iam_for_lambda.name
  policy_arn = aws_iam_policy.lambda_logging.arn
}