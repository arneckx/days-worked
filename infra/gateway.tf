data "aws_region" "current" {}
data "aws_caller_identity" "current" {}

resource "aws_api_gateway_rest_api" "backoffice_api" {
  name = "backoffice_api"
}

resource "aws_api_gateway_deployment" "this" {
  rest_api_id = aws_api_gateway_rest_api.backoffice_api.id

  triggers = {
    redeployment = sha1(join(",", concat(
      [
        jsonencode(aws_api_gateway_resource.backoffice),
        jsonencode(aws_api_gateway_resource.backoffice_files),
      ]
    )))
  }

  depends_on = [
    aws_api_gateway_integration.post,
    aws_api_gateway_integration.post_files,
    aws_api_gateway_integration.options,
    aws_api_gateway_integration.options_files,
  ]
}

resource "aws_api_gateway_method" "post" {
  rest_api_id   = aws_api_gateway_rest_api.backoffice_api.id
  resource_id   = aws_api_gateway_resource.backoffice.id
  http_method   = "POST"
  authorization = "COGNITO_USER_POOLS"
  authorizer_id = aws_api_gateway_authorizer.api_authorizer.id

  request_parameters = {
    "method.request.path.proxy" = true
  }
}

resource "aws_api_gateway_method" "post_files" {
  rest_api_id   = aws_api_gateway_rest_api.backoffice_api.id
  resource_id   = aws_api_gateway_resource.backoffice_files.id
  http_method   = "POST"
  authorization = "COGNITO_USER_POOLS"
  authorizer_id = aws_api_gateway_authorizer.api_authorizer.id

  request_parameters = {
    "method.request.path.proxy" = true
  }
}

resource "aws_api_gateway_method" "options" {
  rest_api_id   = aws_api_gateway_rest_api.backoffice_api.id
  resource_id   = aws_api_gateway_resource.backoffice.id
  http_method   = "OPTIONS"
  authorization = "NONE"
}

resource "aws_api_gateway_method" "options_files" {
  rest_api_id   = aws_api_gateway_rest_api.backoffice_api.id
  resource_id   = aws_api_gateway_resource.backoffice_files.id
  http_method   = "OPTIONS"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "options" {
  rest_api_id = aws_api_gateway_rest_api.backoffice_api.id
  resource_id = aws_api_gateway_resource.backoffice.id
  http_method = aws_api_gateway_method.options.http_method

  type = "MOCK"

  request_templates = {
    "application/json" = jsonencode(
      {
        statusCode = 200
      }
    )
  }
}

resource "aws_api_gateway_integration" "options_files" {
  rest_api_id = aws_api_gateway_rest_api.backoffice_api.id
  resource_id = aws_api_gateway_resource.backoffice_files.id
  http_method = aws_api_gateway_method.options_files.http_method

  type = "MOCK"

  request_templates = {
    "application/json" = jsonencode(
      {
        statusCode = 200
      }
    )
  }
}

resource "aws_api_gateway_integration_response" "options_response_200" {
  rest_api_id = aws_api_gateway_rest_api.backoffice_api.id
  resource_id = aws_api_gateway_resource.backoffice.id
  http_method = aws_api_gateway_method.options.http_method
  status_code = 200

  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin"  = "'*'"
    "method.response.header.Access-Control-Allow-Methods" = "'GET,POST,OPTIONS'"
    "method.response.header.Access-Control-Allow-Headers" = "'content-type,authorization,x-backoffice-command'"
  }

  depends_on = [aws_api_gateway_integration.post]
}

resource "aws_api_gateway_integration_response" "options_files_response_200" {
  rest_api_id = aws_api_gateway_rest_api.backoffice_api.id
  resource_id = aws_api_gateway_resource.backoffice_files.id
  http_method = aws_api_gateway_method.options_files.http_method
  status_code = 200

  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin"  = "'*'"
    "method.response.header.Access-Control-Allow-Methods" = "'GET,POST,OPTIONS'"
    "method.response.header.Access-Control-Allow-Headers" = "'content-type,authorization,x-backoffice-command'"
  }

  depends_on = [aws_api_gateway_integration.post_files]
}

resource "aws_api_gateway_method_response" "options_response_200" {
  rest_api_id = aws_api_gateway_rest_api.backoffice_api.id
  resource_id = aws_api_gateway_resource.backoffice.id
  http_method = aws_api_gateway_method.options.http_method
  status_code = 200

  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin"  = true,
    "method.response.header.Access-Control-Allow-Methods" = true,
    "method.response.header.Access-Control-Allow-Headers" = true,
  }
  response_models = { "application/json" = "Empty" }
}

resource "aws_api_gateway_method_response" "options_files_response_200" {
  rest_api_id = aws_api_gateway_rest_api.backoffice_api.id
  resource_id = aws_api_gateway_resource.backoffice_files.id
  http_method = aws_api_gateway_method.options_files.http_method
  status_code = 200

  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin"  = true,
    "method.response.header.Access-Control-Allow-Methods" = true,
    "method.response.header.Access-Control-Allow-Headers" = true,
  }
  response_models = { "application/json" = "Empty" }
}

resource "aws_api_gateway_integration" "post" {
  rest_api_id             = aws_api_gateway_rest_api.backoffice_api.id
  resource_id             = aws_api_gateway_resource.backoffice.id
  http_method             = aws_api_gateway_method.post.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_alias.backoffice_lambda_alias.invoke_arn
}

resource "aws_api_gateway_integration" "post_files" {
  rest_api_id             = aws_api_gateway_rest_api.backoffice_api.id
  resource_id             = aws_api_gateway_resource.backoffice_files.id
  http_method             = aws_api_gateway_method.post.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_alias.files_lambda_alias.invoke_arn
}


resource "aws_api_gateway_stage" "api" {
  deployment_id = aws_api_gateway_deployment.this.id
  rest_api_id   = aws_api_gateway_rest_api.backoffice_api.id
  stage_name    = "api"

  xray_tracing_enabled = false

  access_log_settings {
    destination_arn = "arn:aws:logs:eu-central-1:020814835884:log-group:API-Gateway-Execution-Logs_xxr0b4kz05/api"
    format          = "$context.identity.sourceIp $context.identity.caller $context.identity.user [$context.requestTime] \"$context.httpMethod $context.resourcePath $context.protocol\" $context.status $context.responseLength $context.requestId"
  }
}

resource "aws_api_gateway_resource" "backoffice" {
  rest_api_id = aws_api_gateway_rest_api.backoffice_api.id
  parent_id   = aws_api_gateway_rest_api.backoffice_api.root_resource_id
  path_part   = "backoffice"
}

resource "aws_api_gateway_resource" "backoffice_files" {
  rest_api_id = aws_api_gateway_rest_api.backoffice_api.id
  parent_id   = aws_api_gateway_resource.backoffice.id
  path_part   = "files"
}

resource "aws_iam_role_policy_attachment" "apigateway_cloudwatch" {
  role       = aws_iam_role.iam_for_api_gateway.id
  policy_arn = data.aws_iam_policy.api_gateway_cloudwatch.arn
}

resource "aws_api_gateway_account" "events_apigateway" {
  cloudwatch_role_arn = aws_iam_role.iam_for_api_gateway.arn
}

resource "aws_cloudwatch_log_group" "this" {
  name = "API-Gateway-Execution-Logs_${aws_api_gateway_rest_api.backoffice_api.id}/${aws_api_gateway_stage.api.stage_name}"
}

resource "aws_iam_role" "iam_for_api_gateway" {
  name = "iam_for_api_gateway"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "apigateway.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

data "aws_iam_policy" "api_gateway_cloudwatch" {
  arn = "arn:aws:iam::aws:policy/service-role/AmazonAPIGatewayPushToCloudWatchLogs"
}

resource "aws_lambda_permission" "apigw_lambda_get_messages" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.backoffice.function_name
  principal     = "apigateway.amazonaws.com"

  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  source_arn = "arn:aws:execute-api:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:${aws_api_gateway_rest_api.backoffice_api.id}/*/${aws_api_gateway_method.post.http_method}${aws_api_gateway_resource.backoffice.path}"
  qualifier     = aws_lambda_alias.backoffice_lambda_alias.name
}

resource "aws_lambda_permission" "apigw_lambda_post_file" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.files.function_name
  principal     = "apigateway.amazonaws.com"

  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  source_arn = "arn:aws:execute-api:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:${aws_api_gateway_rest_api.backoffice_api.id}/*/${aws_api_gateway_method.post_files.http_method}${aws_api_gateway_resource.backoffice_files.path}"
  qualifier     = aws_lambda_alias.files_lambda_alias.name
}

resource "aws_api_gateway_authorizer" "api_authorizer" {
  name          = "BackofficeCognitoUserPoolAuthorizer"
  type          = "COGNITO_USER_POOLS"
  rest_api_id   = aws_api_gateway_rest_api.backoffice_api.id
  provider_arns = [aws_cognito_user_pool.backoffice_pool.arn]
}