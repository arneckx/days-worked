output "backoffice_api" {
  value = "${aws_api_gateway_deployment.this.invoke_url}api${aws_api_gateway_resource.backoffice.path}"
}