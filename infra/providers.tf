provider "aws" {

  region = "eu-central-1"

  default_tags {
    tags = {
      Owner       = "arneckx"
      Application = "backoffice"
      Project     = "backoffice"
      Terraform   = "https://gitlab.com/arneckx/backoffice/infra"
    }
  }
}
