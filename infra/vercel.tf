resource "vercel_project" "backoffice" {
  name      = "backoffice"
  framework = "nextjs"
  git_repository = {
    production_branch = "main"
    repo              = "arneckx/backoffice"
    type              = "gitlab"
  }
  root_directory = "frontend"

  lifecycle {
    prevent_destroy = true
  }
}

resource "vercel_project_domain" "www_backoffice_arneckx" {
  project_id = vercel_project.backoffice.id
  domain     = "backoffice.arneckx.be"

  lifecycle {
    prevent_destroy = true
  }
}

resource "vercel_project_environment_variable" "backoffice_api_url" {
  project_id = vercel_project.backoffice.id
  key        = "NEXT_PUBLIC_BACKOFFICE_API_URL"
  value      = "${aws_api_gateway_deployment.this.invoke_url}api${aws_api_gateway_resource.backoffice.path}"
  target = [
    "production",
    "preview",
    "development",
  ]

  lifecycle {
    prevent_destroy = true
  }
}