terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.26"
    }
    vercel = {
      source  = "vercel/vercel"
      version = "~> 0.16"
    }
  }
}
