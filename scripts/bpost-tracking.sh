#!/usr/bin/env bash

# usage: ./bpost-tracking.sh 323211191400073968692030

watch -n 10 -d "curl --location 'https://track.bpost.cloud/track/itemonroundstatus?itemIdentifier=$1&postalCode=9240' | jq '{\"nr_of_stops_until_target\" : .itemOnRoundStatus.nrOfStopsUntilTarget.[0], \"last_location\": (.itemOnRoundStatus.lastKnownLocation.[0].lat[0] + \",\" + .itemOnRoundStatus.lastKnownLocation.[0].long[0])}'"