#!/bin/bash

# Check if an input file is provided
if [ $# -eq 0 ]; then
    echo "Usage: $0 <input_csv_file>"
    exit 1
fi

# Variable to store all unique characters
all_unique_chars=""

# Read the input CSV file line by line
while IFS= read -r line; do
    # Extract unique characters from the line
    unique_chars=$(echo "$line" | grep -o . | sort -u | tr -d '\n')

    # Output the attribute name and its unique characters
    echo "Attribute: $line - Unique characters: $(printf '%b' "$unique_chars")"

    # Append unique characters to the variable
    all_unique_chars="${all_unique_chars}${unique_chars}"
done < "$1"

# Extract all unique characters encountered
final_unique_chars=$(echo "$all_unique_chars" | grep -o . | sort -u | tr -d '\n')

# Output all unique characters at the end of the run
echo "All unique characters encountered: $(printf '%b' "$final_unique_chars")"
